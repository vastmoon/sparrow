package com.vastmoon.sparrow.core.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 提供操作applicationContext的接口
 *
 * @author yousuf 2020/12/25 11:47
 **/
public abstract class AbstractSparrowContext implements ApplicationContextAware, InitializingBean {
    protected ApplicationContext applicationContext;

    /**
     * 通过接口获取spring上下文
     * @param applicationContext 上下文
     * @throws BeansException 异常
     */
    @Override
    @SuppressWarnings("NullableProblems")
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
