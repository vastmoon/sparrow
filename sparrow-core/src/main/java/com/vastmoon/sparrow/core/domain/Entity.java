package com.vastmoon.sparrow.core.domain;

import java.io.Serializable;

/**
 * 实体接口
 *
 * @author yousuf 2020/12/25 14:27
 **/
public interface Entity extends Serializable {
}
