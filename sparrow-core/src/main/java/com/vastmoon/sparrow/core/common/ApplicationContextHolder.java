package com.vastmoon.sparrow.core.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;

/**
 * applicationContext 工具类
 *
 * @author yousuf 2020/12/25 10:48
 **/
public class ApplicationContextHolder implements ApplicationContextAware, DisposableBean {
    private static ApplicationContext applicationContext = null;

    /**
     * 对外暴露 applicationContext
     * @see ApplicationContext
     * @return applicationContext
     */
    public static ApplicationContext get() {
        return applicationContext;
    }

    /**
     * 获取资源信息
     * @param fileName 文件名称
     * @return 资源
     */
    public static Resource getResource(String fileName) {
        return get().getResource(fileName);
    }
    @Override
    public void destroy() throws Exception {
        ApplicationContextHolder.applicationContext = null;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationContextHolder.applicationContext = applicationContext;
    }
}
