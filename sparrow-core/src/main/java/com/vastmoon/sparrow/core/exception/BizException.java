package com.vastmoon.sparrow.core.exception;

import com.vastmoon.sparrow.client.dto.rest.RestCode;
import com.vastmoon.sparrow.core.exception.framework.BaseException;
import com.vastmoon.sparrow.core.exception.framework.FrameworkCode;

/**
 * 业务异常
 *
 * @author yousuf 2020/12/23 09:09
 **/
public class BizException extends BaseException {
    private static final long serialVersionUID = 7546873800164703139L;

    public BizException(String errorMessage) {
        super(errorMessage);
        this.setCode(FrameworkCode.BIZ_ERROR);
    }

    public BizException(String errorMessage, Throwable e) {
        super(errorMessage, e);
        this.setCode(FrameworkCode.BIZ_ERROR);
    }

    public BizException(RestCode code, String errMessage) {
        super(errMessage);
        this.setCode(code);
    }
}
