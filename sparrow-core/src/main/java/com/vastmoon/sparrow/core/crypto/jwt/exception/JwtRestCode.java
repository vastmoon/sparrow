package com.vastmoon.sparrow.core.crypto.jwt.exception;

import com.vastmoon.sparrow.client.dto.rest.RestCode;
import lombok.Getter;

/**
 * jwt 状态码
 *
 * @author yousuf 2021/1/8 11:52
 **/
@Getter
public enum JwtRestCode implements RestCode {
    /**jwt 业务码*/
    RSA_NAME_NOT_FIND("rsa_name_not_find", "rsa key not found"),
    TOKEN_SING_ERROR("token_sing_error", "The token signature is illegal!"),
    TOKEN_EXPIRED("token_expired", "jwt token is expired"),
    ;
    private final String code;
    private final String message;

    JwtRestCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
