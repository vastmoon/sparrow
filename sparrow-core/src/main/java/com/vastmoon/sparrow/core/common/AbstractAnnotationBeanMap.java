package com.vastmoon.sparrow.core.common;

import com.google.common.collect.Maps;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * 通过注解来组织相关操作的策略
 *
 * @author yousuf 2020/12/25 11:50
 **/
public abstract class AbstractAnnotationBeanMap<A extends Annotation,B> extends AbstractSparrowContext {
    /**
     * 注解类型.
     *
     * @return 注解class
     */
    protected abstract Class<A> getAnnotation();

    /**
     * 提供给实现类操作注解获取到的bean的接口.
     * @param annotationBeanMap 标有注解的bean的集合
     */
    protected abstract void annotationBeanMap(Map<A, B> annotationBeanMap);

    @Override
    @SuppressWarnings("unchecked")
    public void afterPropertiesSet() throws Exception {
        Map<A, B> annotationBeanMap = Maps.newHashMap();
        Map<String, Object> beanMap = applicationContext.getBeansWithAnnotation(getAnnotation());
        beanMap.values().forEach(bean -> {
            A annotation = (A) AnnotationUtils.findAnnotation(bean.getClass(), getAnnotation());
            annotationBeanMap.put(annotation, (B) bean);
        });
        annotationBeanMap(annotationBeanMap);
    }
}
