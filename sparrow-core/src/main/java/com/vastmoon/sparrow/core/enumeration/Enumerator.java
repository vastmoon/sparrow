package com.vastmoon.sparrow.core.enumeration;

import com.vastmoon.sparrow.core.exception.framework.FrameworkException;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * 枚举接口
 *
 * @author yousuf 2020/12/22 17:36
 **/
public interface Enumerator<T> {
    /**
     * 枚举代码，一般用于持久化枚举
     * @return code码
     */
    T getCode();

    /**
     * 枚举类辅助信息 一般用于前端显示或者说明枚举用图
     * @return 辅助类型
     */
    String getText();

    /**
     * 根据类型查询枚举信息
     * @param type 类型
     * @param predicate 操作
     * @param <T> 枚举类型
     * @return 返回对应的枚举
     */
    static <T extends Enum<?> & Enumerator<?>> Optional<T> find(Class<T> type, Predicate<T> predicate) {
        if (type.isEnum()) {
            return Arrays.stream(type.getEnumConstants())
                    .filter(predicate).findFirst();
        }
        return Optional.empty();
    }

    /**
     * 根据 code 找到对应的枚举
     * @see Enumerator#find(Class, Predicate)
     * @param type 类型
     * @param code code
     * @param <T> 枚举类型
     * @return T 类型的枚举
     */
    static <T extends Enum<?> & Enumerator<?>> Optional<T> findOptionalByCode(Class<T> type, Object code) {
        return find(type, e -> e.getCode() == code || e.getCode().equals(code)
                || String.valueOf(e.getCode()).equalsIgnoreCase(String.valueOf(code)));
    }

    /**
     * 根据 text 查找对应的枚举
     * @see Enumerator#find(Class, Predicate)
     * @param type 类型
     * @param text text
     * @param <T> 枚举类型
     * @return T 对应的枚举
     */
    static <T extends Enum<?> & Enumerator<?>> Optional<T> findOptionalByText(Class<T> type, String text) {
        return find(type, e -> e.getText().equalsIgnoreCase(text));
    }

    /**
     * 根据code查找枚举
     * @see Enumerator#findOptionalByCode(Class, Object)
     * @param type 类型
     * @param code 枚举code
     * @param <T> 枚举类型
     * @return T 类型的枚举
     */
    static <T extends Enum<?> & Enumerator<?>> T findByCode(Class<T> type, Object code) {
        return findOptionalByCode(type, code)
                .orElseThrow(() -> new FrameworkException("enumeration conversion exception"));
    }

    /**
     * 根据text查询枚举类型
     * @see Enumerator#findOptionalByText(Class, String)
     * @param type 类型
     * @param text text
     * @param <T> 枚举类型
     * @return T 类型的枚举
     */
    static <T extends Enum<?> & Enumerator<?>> T findByText(Class<T> type, String text) {
        return findOptionalByText(type, text)
                .orElseThrow(() -> new FrameworkException("枚举转换异常"));
    }
}
