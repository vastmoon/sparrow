package com.vastmoon.sparrow.core.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vastmoon.sparrow.client.dto.rest.RestBody;
import com.vastmoon.sparrow.client.dto.rest.RestMap;
import com.vastmoon.sparrow.client.dto.rest.RestMapFactory;
import com.vastmoon.sparrow.core.security.CustomizeSecurityCode;
import com.vastmoon.sparrow.core.security.util.AuthenticationExceptionUtils;
import com.vastmoon.sparrow.core.util.WebUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 未登录异常处理
 *
 * @author yousuf 2021/1/5 15:24
 **/
@Slf4j
@RequiredArgsConstructor
public class CustomizeAuthenticationEntryPoint implements AuthenticationEntryPoint {
    private final ObjectMapper objectMapper;
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        Map<String, Object> errors = AuthenticationExceptionUtils.ofAuthenticationException(authException.getClass());
        RestMap restMap = RestMapFactory.ofErrors(RestBody.failure(CustomizeSecurityCode.UNAUTHORIZED), errors);
        WebUtils.responseJsonWriter(response, objectMapper.writeValueAsString(restMap.toMap()), HttpServletResponse.SC_UNAUTHORIZED);
    }
}
