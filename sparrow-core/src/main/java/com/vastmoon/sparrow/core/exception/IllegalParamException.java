package com.vastmoon.sparrow.core.exception;

import com.google.common.collect.Maps;
import com.vastmoon.sparrow.client.dto.rest.*;
import com.vastmoon.sparrow.core.exception.framework.FrameworkCode;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

/**
 * 参数异常
 *
 * @author yousuf 2020/12/23 09:21
 **/
public class IllegalParamException extends BizException {
    private static final long serialVersionUID = -6590512385045595648L;

    public IllegalParamException(String errorMessage) {
        super(errorMessage);
        super.setCode(FrameworkCode.ILLEGAL_PARAM_ERROR);
    }

    public IllegalParamException(String errorMessage, Throwable e) {
        super(errorMessage, e);
        super.setCode(FrameworkCode.ILLEGAL_PARAM_ERROR);
    }

    public IllegalParamException(RestCode code, String errMessage) {
        super(code, errMessage);
    }

    public IllegalParamException(String errorMessage, Map<String, Object> errors) {
        this(errorMessage);
        super.setErrors(errors);
    }

    public IllegalParamException(RestCode code, String errMessage, Map<String, Object> errors) {
        super(code, errMessage);
        super.setErrors(errors);
    }

    public IllegalParamException addError(String filed, String errorMessage) {
        if (MapUtils.isEmpty(super.getErrors())) {
            super.setErrors(Maps.newHashMap());
        }
        super.getErrors().put(filed, errorMessage);
        return this;
    }

    public RestMap toRestMap() {
        Rest<Object> rest = RestBody.failure(this.getCode());
        if (MapUtils.isEmpty(this.getErrors())) {
            return RestMapFactory.of(rest);
        }
        if (this.getErrors().size() == 1) {
            Map.Entry<String, Object> next = this.getErrors().entrySet().stream().iterator().next();
            return RestMapFactory.of(rest, next.getKey(), next.getValue());
        }
        return RestMapFactory.ofErrors(rest, this.getErrors());
    }
}
