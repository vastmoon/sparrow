package com.vastmoon.sparrow.core.enumeration;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * true or false 枚举
 *
 * @author yousuf 2020/12/23 15:26
 **/
@Getter
public enum TrueOrFalseEnum implements Enumerator<Integer> {
    /**true or false 枚举*/
    TRUE(1, "true"),
    FALSE(0, "false");

    private final Integer code;
    private final String text;

    TrueOrFalseEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public static TrueOrFalseEnum parse(Boolean code) {
        return code ? TRUE : FALSE;
    }

    public static TrueOrFalseEnum parse(int code) {
        return code == 1 ? TRUE : FALSE;
    }
    
    public static TrueOrFalseEnum parse(String text) {
        return StringUtils.equalsIgnoreCase(text, TrueOrFalseEnum.TRUE.getText()) ? TRUE : FALSE;
    }

    public Boolean convert() {
        return this == TrueOrFalseEnum.TRUE ? Boolean.TRUE : Boolean.FALSE;
    }
}
