package com.vastmoon.sparrow.core.crypto.jwt;

import java.time.Duration;
import java.util.Map;
import java.util.Set;

/**
 * jwt payload 生成器
 *
 * @author yousuf 2021/1/7 10:44
 **/
public class PayloadGenerator {
    private final JwtPayloadBuilder jwtPayloadBuilder;
    private final JwtProperties properties;

    public PayloadGenerator(JwtProperties properties) {
        this.properties = properties;
        this.jwtPayloadBuilder = new JwtPayloadBuilder();
    }

    /**
     * access payload 生成方法
     * @param tokenId {@link AccessTokenId}
     * @param roles 角色
     * @param additional 附加属性
     * @return payload
     */
    public String accessPayload(TokenId tokenId, Set<String> roles, Map<String, String> additional) {
        return payload(tokenId, this.properties.getAccessExp(), roles, additional);
    }

    /**
     * refreshPayload 生成
     * @param tokenId {@link RefreshTokenId}
     * @param roles 角色
     * @param additional 附加属性
     * @return payload
     */
    public String refreshPayload(TokenId tokenId,Set<String> roles, Map<String, String> additional) {
        return payload(tokenId, this.properties.getRefreshExp(), roles, additional);
    }
    /**
     * 用于生成token
     * @param tokenId 生成token
     * @param roles 权限
     * @param additional 附加属性
     * @return token
     */
    private String payload(TokenId tokenId, Duration exp, Set<String> roles, Map<String, String> additional) {
        return jwtPayloadBuilder
                .iss(properties.getIss())
                .sub(properties.getSub())
                .aud(tokenId.getAud())
                .additional(additional)
                .roles(roles)
                .expTime(exp)
                .builder();
    }

}
