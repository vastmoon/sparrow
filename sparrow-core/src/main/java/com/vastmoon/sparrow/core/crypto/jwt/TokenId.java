package com.vastmoon.sparrow.core.crypto.jwt;

import java.io.Serializable;

/**
 * tokenId
 *
 * @author yousuf 2021/1/7 09:57
 **/
public interface TokenId extends Serializable {
    /**
     * tokenId前缀
     * @return tokenId
     */
    String getPrefix();

    /**
     * 授权用户
     * @return 授权用户
     */
    String getAud();
}
