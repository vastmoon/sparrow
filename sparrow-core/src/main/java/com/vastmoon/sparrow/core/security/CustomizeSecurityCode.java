package com.vastmoon.sparrow.core.security;

import com.vastmoon.sparrow.client.dto.rest.RestCode;
import lombok.Getter;

/**
 * 自定义状态吗
 *
 * @author yousuf 2021/1/5 15:00
 **/
@Getter
public enum CustomizeSecurityCode implements RestCode {
    /**业务码*/
    FORBIDDEN("forbidden", "未授权"),
    UNAUTHORIZED("unauthorized", "未登录"),
    BAD_CREDENTIALS("bad_credentials", "用户名密码错误"),
    INSUFFICIENT("insufficient", "要访问此资源，必须进行完全身份验证"),
    ACCESS_DENIED("access_denied", "非法访问");

    private final String code;
    private final String message;

    CustomizeSecurityCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
