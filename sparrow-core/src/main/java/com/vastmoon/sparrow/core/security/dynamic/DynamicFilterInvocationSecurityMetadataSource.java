package com.vastmoon.sparrow.core.security.dynamic;

import com.google.common.collect.Sets;
import com.vastmoon.sparrow.client.dto.security.MetaResource;
import com.vastmoon.sparrow.client.dto.security.RoleResource;
import com.vastmoon.sparrow.core.security.SecurityConstants;
import com.vastmoon.sparrow.core.security.annotation.Skip;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.ExpressionBasedFilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 动态权限扩展
 * 这里参考了 {@link ExpressionBasedFilterInvocationSecurityMetadataSource}的实现
 *
 * @author yousuf 2021/1/7 15:49
 **/
@Slf4j
@RequiredArgsConstructor
public class DynamicFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource, InitializingBean, ApplicationContextAware {
    private final RequestMatcherConverter matcherConverter;
    private final MetaResourceProcessor resourceProcessor;
    private final RoleProcessor roleProcessor;
    private Set<RequestMatcher> requestMatchers;
    private Set<RoleResource> skips;
    private ApplicationContext applicationContext;
    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        // 获取当前的请求
        final HttpServletRequest request = ((FilterInvocation) object).getRequest();

        // 拿到其中一个  没有就算非法访问
        RequestMatcher reqMatcher = this.requestMatchers.stream()
                .filter(requestMatcher -> requestMatcher.matches(request))
                .findAny()
                .orElseThrow(() -> new AccessDeniedException("非法访问"));
        AntPathRequestMatcher antPathRequestMatcher = (AntPathRequestMatcher) reqMatcher;
        // 根据pattern 获取该pattern被授权给的角色
        String pattern = antPathRequestMatcher.getPattern();
        MetaResource metaResource = new MetaResource(pattern, request.getMethod());
        Set<String> roles = roleProcessor.queryRoleByPattern(metaResource);
        if (CollectionUtils.isEmpty(roles)) {
            roles = skipsPermission(metaResource);
        }
        return CollectionUtils.isEmpty(roles) ? null : SecurityConfig.createList(roles.toArray(new String[0]));
    }

    /**
     * 通过注解来判断是否匿名访问或者只需要登录权限就可以访问
     * @param resource 资源
     * @return roles
     */
    private Set<String> skipsPermission(MetaResource resource) {
        return this.skips.stream().filter(role -> Objects.equals(role.getResource(), resource))
                .map(RoleResource::getRole)
                .collect(Collectors.toSet());
    }
    /**
     * 初始化不需要分配权限的资源
     */
    private void initSkipPermission() {
        this.skips = Sets.newHashSet();
        // 这里可以放一个抽象接口来获取  request 配置的 ant pattern 所有权限的集合
        this.requestMatchers = matcherConverter.convertToRequestMatcher(resourceProcessor.queryPatternsAndMethods());
        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> requestMap = mapping.getHandlerMethods();
        requestMap.forEach((info, method) -> {
            Skip skip = method.getMethodAnnotation(Skip.class);
            if (Objects.nonNull(skip)) {
                info.getPatternsCondition().getPatterns().forEach(url -> {
                    info.getMethodsCondition().getMethods().forEach(requestMethod -> {
                        MetaResource resource = new MetaResource(url, requestMethod.name());
                        this.requestMatchers.add(new AntPathRequestMatcher(url, requestMethod.name()));
                        if (skip.logged()) {
                            skips.add(new RoleResource(SecurityConstants.ROLE_SKIP, resource));
                        } else {
                            skips.add(new RoleResource(SecurityConstants.ROLE_ANONYMOUS, resource));
                        }

                    });
                });
            }
        });
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        Set<String> roles = roleProcessor.queryAllAvailable();
        return CollectionUtils.isEmpty(roles) ? null : SecurityConfig.createList(roles.toArray(new String[0]));
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return FilterInvocation.class.isAssignableFrom(clazz);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initSkipPermission();
    }

    @Override
    @SuppressWarnings("NullableProblems")
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
