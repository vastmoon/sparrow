package com.vastmoon.sparrow.core.exception.handler;

import com.google.common.collect.Lists;
import com.vastmoon.sparrow.core.util.WebUtils;
import com.vastmoon.sparrow.client.dto.rest.Rest;
import com.vastmoon.sparrow.client.dto.rest.RestBody;
import com.vastmoon.sparrow.client.dto.rest.RestMap;
import com.vastmoon.sparrow.client.dto.rest.RestMapFactory;
import com.vastmoon.sparrow.core.exception.BizException;
import com.vastmoon.sparrow.core.exception.IllegalParamException;
import com.vastmoon.sparrow.core.exception.framework.BaseException;
import com.vastmoon.sparrow.core.exception.framework.FrameworkCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Map;

/**
 * 统一异常处理类
 *
 * @author yousuf 2020/12/28 11:44
 **/
@Slf4j
public abstract class BaseExceptionHandler {
    /**
     * 参数异常信息封装
     * @param request 请求
     * @param ex 异常
     * @return rest风格信息
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Map<String, Object> paramError(HttpServletRequest request, MethodArgumentNotValidException ex) {
        // 参数异常返回400
        log.warn("传入参数不合法, url({}), method({}), exception: {}",
                WebUtils.requestFullUrl(request),
                request.getMethod(),
                ExceptionUtils.getMessage(ex));
        FieldError fieldError = ex.getBindingResult().getFieldErrors().get(0);
        return RestMapFactory.of(RestBody.failure(FrameworkCode.ILLEGAL_PARAM_ERROR),
                fieldError.getField(),
                fieldError.getDefaultMessage())
                .toMap();
    }

    /**
     * 参数异常信息封装
     * @param request 请求
     * @param ex 异常
     * @return rest风格信息
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = ConstraintViolationException.class)
    public Map<String, Object> paramError(HttpServletRequest request, ConstraintViolationException ex) {
        // 参数异常返回400
        log.warn("传入参数不合法, url({}), method({}), exception: {}",
                WebUtils.requestFullUrl(request),
                request.getMethod(),
                ExceptionUtils.getMessage(ex));
        ConstraintViolation<?> constraintViolation = Lists.newArrayList(ex.getConstraintViolations()).get(0);
        return RestMapFactory.of(RestBody.failure(FrameworkCode.ILLEGAL_PARAM_ERROR),
                constraintViolation.getPropertyPath().toString(),
                constraintViolation.getMessage())
                .toMap();
    }

    /**
     * 自定义参数异常
     * @param request request
     * @param ex 自定义参数异常
     * @return rest风格信息
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = IllegalParamException.class)
    public Map<String, Object> paramError(HttpServletRequest request, IllegalParamException ex) {
        // 参数异常返回400
        log.warn("传入参数不合法, code({}), message({}), url({}), method({}), exception: {}",
                ex.getCode(),
                ex.getMessage(),
                WebUtils.requestFullUrl(request),
                request.getMethod(),
                ExceptionUtils.getMessage(ex));
        RestMap restMap = ex.toRestMap();
        return restMap.toMap();
    }

    /**
     * 业务异常 业务异常也返回400
     * @param request 请求
     * @param ex 业务异常
     * @return 业务异常
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = BizException.class)
    public Rest<Void> bizError(HttpServletRequest request, BaseException ex) {
        // 业务逻辑异常返回 400
        log.warn("业务异常, code({}), message({}), url({}), method({}), exception: {}",
                ex.getCode(),
                ex.getMessage(),
                WebUtils.requestFullUrl(request),
                request.getMethod(),
                ExceptionUtils.getMessage(ex));
        return RestBody.failure(ex.getCode());
    }

    /**
     * 没有处理的异常，未知异常
     * @param request request
     * @param ex 异常
     * @return 系统异常
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = BaseException.class)
    public Rest<Void> sparrowError(HttpServletRequest request, BaseException ex) {
        log.warn("系统异常, code({}), message({}), url({}), method({}), exception: {}",
                ex.getCode(),
                ex.getMessage(),
                WebUtils.requestFullUrl(request),
                request.getMethod(),
                ExceptionUtils.getMessage(ex));
        return RestBody.failure(ex.getCode());
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public Rest<Void> sparrowError(HttpServletRequest request, HttpRequestMethodNotSupportedException ex) {
        log.warn("请求方式错误, url({}), method({}), exception: {}",
                WebUtils.requestFullUrl(request),
                request.getMethod(),
                ExceptionUtils.getMessage(ex));
        return RestBody.failure(FrameworkCode.METHOD_NOT_ALLOWED);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public Rest<Void> jsonConvertError(HttpServletRequest request, HttpMessageNotReadableException ex) {
        log.warn("请求格式错误, url({}), method({}), exception: {}",
                WebUtils.requestFullUrl(request),
                request.getMethod(), ExceptionUtils.getMessage(ex));
        return RestBody.failure(FrameworkCode.JSON_CONVERT_ERROR);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    public Rest<Void> error(HttpServletRequest request, Exception ex) {
        log.error("未捕获的异常, url({}), method({}), exception: {}",
                request.getServletPath(),
                request.getMethod(), ExceptionUtils.getStackTrace(ex));
        return RestBody.failure(FrameworkCode.SYS_ERROR);
    }
}
