package com.vastmoon.sparrow.core.enumeration;

import lombok.Getter;

/**
 * http 请求枚举
 *
 * @author yousuf 2020/12/24 16:29
 **/
@Getter
public enum RequestMethodEnum implements Enumerator<Integer> {
    /**http请求方式枚举*/
    NONE(0, "N/A"),
    GET(1, "GET"),
    POST(2, "POST"),
    PUT(3, "PUT"),
    PATCH(4, "PATCH"),
    DELETE(5, "DELETE"),
    HEAD(6, "HEAD"),
    OPTIONS(7, "OPTIONS"),
    TRACE(8, "TRACE"),
    CONNECT(9, "CONNECT"),
    ALL(10, "ALL"),
    ;

    private final Integer code;
    private final String text;

    RequestMethodEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     *  通过 text 转换 为 RequestMethodEnum
     * @see Enumerator#findByText(Class, String)
     * @param method 请求名称
     * @return RequestMethodEnum 类型枚举
     */
    public static RequestMethodEnum of(String method) {
        return Enumerator.findByText(RequestMethodEnum.class, method);
    }
}
