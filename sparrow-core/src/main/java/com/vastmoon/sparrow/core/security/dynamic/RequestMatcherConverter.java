package com.vastmoon.sparrow.core.security.dynamic;

import com.vastmoon.sparrow.client.dto.security.MetaResource;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Set;

/**
 * requestMatcher生成器
 *
 * @author yousuf 2021/1/7 15:57
 **/
public interface RequestMatcherConverter {
    /**
     * 把权限资源转换为 requestMatcher
     * @param metaResources 资源
     * @return 转换后的资源
     */
    Set<RequestMatcher> convertToRequestMatcher(Set<MetaResource> metaResources);
}
