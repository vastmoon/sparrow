package com.vastmoon.sparrow.core.security.filter;

import com.vastmoon.sparrow.core.security.login.LoginPostProcessor;
import com.vastmoon.sparrow.core.security.login.LoginPostProcessorManger;
import com.vastmoon.sparrow.core.security.login.ParameterRequestWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY;
import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY;

/**
 * 登录方式前置过滤器
 *
 * @author yousuf 2021/1/5 13:46
 **/
@RequiredArgsConstructor
public class PreLoginFilter extends GenericFilter implements InitializingBean {
    private static final long serialVersionUID = -4259985393884608620L;
    private static final String LOGIN_TYPE_KEY = "loginType";

    private final String loginProcessingUrl;
    private final LoginPostProcessorManger postProcessorManger;
    private RequestMatcher requiresAuthenticationRequestMatcher;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        ParameterRequestWrapper parameterRequestWrapper = new ParameterRequestWrapper((HttpServletRequest) request);
        if (requiresAuthenticationRequestMatcher.matches((HttpServletRequest) request)) {
            String loginType = parameterRequestWrapper.getParameter(LOGIN_TYPE_KEY);
            LoginPostProcessor loginPostProcessor = postProcessorManger.getLoginPostProcessor(loginType);
            String username = loginPostProcessor.obtainUsername(request);
            String password = loginPostProcessor.obtainPassword(request);

            parameterRequestWrapper.setAttribute(SPRING_SECURITY_FORM_USERNAME_KEY, username);
            parameterRequestWrapper.setAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY, password);

        }
        chain.doFilter(parameterRequestWrapper, response);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(loginProcessingUrl, "loginProcessingUrl must not be null");
        requiresAuthenticationRequestMatcher = new AntPathRequestMatcher(loginProcessingUrl, "POST");
    }
}
