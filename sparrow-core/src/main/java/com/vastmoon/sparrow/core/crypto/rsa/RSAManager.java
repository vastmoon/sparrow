package com.vastmoon.sparrow.core.crypto.rsa;

import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * RSA 控制类
 *
 * @author yousuf 2020/12/31 11:28
 **/
@Slf4j
@RequiredArgsConstructor
public class RSAManager implements InitializingBean {
    private final Set<RSAProperties> properties;
    private Map<String, RSAStore> rsaHolder;
    @Override
    public void afterPropertiesSet() throws Exception {
        rsaHolder = Maps.newHashMap();
        properties.forEach(rsaProperties -> register(new RSAStore(rsaProperties)));
    }

    /**
     * 获取密码
     * @param name 名称
     * @return 秘钥信息
     */
    public Optional<RSAStore> get(String name) {
        return Optional.ofNullable(rsaHolder.get(name));
    }

    /**
     * 注册秘钥
     * @param rsaStore 秘钥
     */
    public void register(RSAStore rsaStore) {
        if (rsaHolder.containsKey(rsaStore.getName())) {
            log.warn("该秘钥[{}]已经存在，现在覆盖秘钥", rsaStore.getName());
        }
        rsaHolder.put(rsaStore.getName(), rsaStore);
    }
}
