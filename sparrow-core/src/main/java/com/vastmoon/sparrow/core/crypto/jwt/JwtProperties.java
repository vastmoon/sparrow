package com.vastmoon.sparrow.core.crypto.jwt;

import lombok.Data;

import java.time.Duration;

/**
 * jwt配置项
 *
 * @author yousuf 2021/1/6 12:22
 **/
@Data
public class JwtProperties {
    private boolean enabled;
    /**jwt rsa名称*/
    private String jwtRsaName;
    /**
     * jwt签发者
     **/
    private String iss;
    /**
     * jwt所面向的用户
     **/
    private String sub;
    /**
     * access jwt token 有效天数
     */
    private Duration accessExp = Duration.parse("PT30M");
    /**
     * refresh jwt token 有效天数
     */
    private Duration refreshExp = Duration.parse("PT60M");
}
