package com.vastmoon.sparrow.core.security.login;

import com.google.common.collect.Maps;
import com.vastmoon.sparrow.core.common.AbstractAnnotationBeanMap;

import java.util.Map;

/**
 * 登录方式管理器
 *
 * @author yousuf 2021/1/5 12:15
 **/
public class LoginPostProcessorManger extends AbstractAnnotationBeanMap<LoginType, LoginPostProcessor> {
    private final Map<String, LoginPostProcessor> loginPostProcessorHashMap = Maps.newHashMap();
    private LoginPostProcessor defaultPostProcessor;
    @Override
    protected Class<LoginType> getAnnotation() {
        return LoginType.class;
    }

    @Override
    protected void annotationBeanMap(Map<LoginType, LoginPostProcessor> annotationBeanMap) {
        annotationBeanMap.forEach((loginType, loginPostProcessor) -> {
            if (loginType.enabled()) {
                loginPostProcessorHashMap.put(loginType.type(), loginPostProcessor);
            }
        });
        this.defaultPostProcessor = new FormLoginPostProcessor();
    }

    /**
     * 获取登录方式处理器
     * @param loginType 登录方式
     * @return 登录方式处理器
     */
    public LoginPostProcessor getLoginPostProcessor(String loginType) {
        return this.loginPostProcessorHashMap.getOrDefault(loginType, this.defaultPostProcessor);
    }
}
