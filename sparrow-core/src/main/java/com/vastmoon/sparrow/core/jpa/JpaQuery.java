package com.vastmoon.sparrow.core.jpa;

import com.vastmoon.sparrow.client.dto.page.OrderDesc;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * jpa查询
 *
 * @author yousuf 2020/12/31 11:55
 **/
@Data
public class JpaQuery {
    private String sql;
    private String countSql;
    private List<Object> paramList;
    private Map<String, Object> aliasParams;
    private Class<?> target;
    private Integer limit;
    private Integer offset;
    private List<OrderDesc> orderDesc;
}
