package com.vastmoon.sparrow.core.crypto.aes;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;

import javax.crypto.SecretKey;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * AES托管类
 *
 * @author yousuf 2020/12/31 11:07
 **/
@Slf4j
@RequiredArgsConstructor
public class AESManager implements InitializingBean {
    private final Set<AesProperties> properties;
    private final AESKeyService aesKeyService;
    private Map<String, AESStore> aesHolder;
    @Override
    public void afterPropertiesSet() throws Exception {
        aesHolder = Maps.newHashMap();
        properties.forEach(aesProperties -> aesHolder.put(aesProperties.getName(),
                new AESStore(aesProperties.getName(), aesProperties.getKey())));
    }

    /**
     * 获取 ase store
     * @param name 名称
     * @return AesStore
     */
    public Optional<AESStore> get(String name) {
        AESStore aesStore = aesHolder.get(name);
        if (Objects.isNull(aesStore) && !aesHolder.containsKey(name)) {
            aesStore = aesKeyService.findAESKey(name);
            aesHolder.put(name, aesStore);
        }
        return Optional.ofNullable(aesStore);
    }

    public void register(AESStore aesStore) {
        if (aesHolder.containsKey(aesStore.getName())) {
            log.warn("该秘钥[{}]已经存在，现在覆盖秘钥", aesStore.getName());
        }
        aesHolder.put(aesStore.getName(), aesStore);
    }

    /**
     * 生成秘钥
     * @param keySize 秘钥长度
     * @return 秘钥
     */
    public static String generateKey(int keySize) {
        SecretKey publicKey = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue(), keySize);
        return Base64.encode(publicKey.getEncoded());
    }
}
