package com.vastmoon.sparrow.core.jpa;

import com.vastmoon.sparrow.client.dto.page.PageData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * jpa增强
 *
 * @author yousuf 2020/12/31 11:53
 **/
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T>,
        PagingAndSortingRepository<T, ID>  {
    /**
     * 通过原生sql查询新 查询条件封装{@link JpaQuery}
     * @param query 查询条件
     * @param <E> 查询实体类型
     * @return list
     */
    <E> List<E> executeNativeQuery(JpaQuery query);

    /**
     * 查询单条记录
     * @param query 查询条件
     * @param <E> 实体
     * @return 实体
     */
    <E> E executeNativeSingleQuery(JpaQuery query);

    /**
     * 查询记录数
     * @param sql 查询sql
     * @param paramList 查询条件
     * @param aliasParams 别名查询参数
     * @return 记录条数
     */
    Long executeCountNativeQuery(String sql, List<Object> paramList, Map<String, Object> aliasParams);

    /**
     * 分页信息
     * @param query 查询条件
     * @param <E> 数据
     * @return 带分页信息的数据
     */
    <E> PageData<E> executePageNativeQuery(JpaQuery query);
}
