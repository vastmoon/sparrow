package com.vastmoon.sparrow.core.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vastmoon.sparrow.client.dto.rest.RestBody;
import com.vastmoon.sparrow.client.dto.rest.RestMap;
import com.vastmoon.sparrow.client.dto.rest.RestMapFactory;
import com.vastmoon.sparrow.core.security.CustomizeSecurityCode;
import com.vastmoon.sparrow.core.util.WebUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 未授权异常
 *
 * @author yousuf 2021/1/5 14:58
 **/
@Slf4j
@RequiredArgsConstructor
public class CustomizeAccessDeniedHandler implements AccessDeniedHandler {
    private final ObjectMapper objectMapper;
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        Map<String, Object> errors = new HashMap<>(3);
        errors.put("uri", request.getRequestURI());
        errors.put("code", CustomizeSecurityCode.ACCESS_DENIED.getCode());
        errors.put("message", CustomizeSecurityCode.ACCESS_DENIED.getMessage());
        RestMap restMap = RestMapFactory.ofErrors(RestBody.failure(CustomizeSecurityCode.FORBIDDEN), errors);
        WebUtils.responseJsonWriter(response, objectMapper.writeValueAsString(restMap.toMap()), HttpServletResponse.SC_FORBIDDEN);
    }
}
