package com.vastmoon.sparrow.core.security.dynamic;

import com.vastmoon.sparrow.client.dto.security.MetaResource;

import java.util.Set;

/**
 * 角色处理器
 *
 * @author yousuf 2021/1/7 16:02
 **/
public interface RoleProcessor {
    /**
     * 根据 url 获取资源
     * @param resource url
     * @return 对应的资源
     */
    Set<String> queryRoleByPattern(MetaResource resource);

    /**
     * 获取所有可用资源
     * @return 可用资源
     */
    Set<String>  queryAllAvailable();
}
