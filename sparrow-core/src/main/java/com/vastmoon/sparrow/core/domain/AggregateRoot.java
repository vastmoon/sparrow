package com.vastmoon.sparrow.core.domain;

import java.io.Serializable;

/**
 * 聚合接口
 *
 * @author yousuf 2020/12/25 14:28
 **/
public interface AggregateRoot extends Serializable {
}
