package com.vastmoon.sparrow.core.security.util;

import com.google.common.collect.Maps;
import com.vastmoon.sparrow.core.security.CustomizeSecurityCode;
import lombok.experimental.UtilityClass;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;

import java.util.HashMap;
import java.util.Map;

/**
 * 异常转换类
 *
 * @author yousuf 2021/1/11 11:05
 **/
@UtilityClass
public class AuthenticationExceptionUtils {
    private final Map<Class<? extends AuthenticationException>,CustomizeSecurityCode> authenticationExceptions;
    static {
        authenticationExceptions = Maps.newHashMap();
        authenticationExceptions.put(BadCredentialsException.class, CustomizeSecurityCode.BAD_CREDENTIALS);
        authenticationExceptions.put(InsufficientAuthenticationException.class, CustomizeSecurityCode.INSUFFICIENT);
    }

    public Map<String, Object> ofAuthenticationException(Class<? extends AuthenticationException> exception) {
        CustomizeSecurityCode code = authenticationExceptions.getOrDefault(exception, CustomizeSecurityCode.UNAUTHORIZED);
        Map<String, Object> errors = new HashMap<>(3);
        errors.put("code", code.getCode());
        errors.put("message", code.getMessage());
        return errors;
    }
}
