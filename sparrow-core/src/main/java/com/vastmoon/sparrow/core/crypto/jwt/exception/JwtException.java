package com.vastmoon.sparrow.core.crypto.jwt.exception;

import com.vastmoon.sparrow.client.dto.rest.RestCode;
import com.vastmoon.sparrow.core.exception.BizException;

/**
 * jwt 异常相关
 *
 * @author yousuf 2021/1/8 11:51
 **/
public class JwtException extends BizException {
    private static final long serialVersionUID = -6065716279138620615L;

    public JwtException(RestCode code, String errMessage) {
        super(code, errMessage);
    }
}
