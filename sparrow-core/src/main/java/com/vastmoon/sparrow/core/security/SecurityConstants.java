package com.vastmoon.sparrow.core.security;

/**
 * 常量
 *
 * @author yousuf 2021/1/19 14:12
 **/
public interface SecurityConstants {
    /**匿名访问*/
    String ROLE_ANONYMOUS="ROLE_ANONYMOUS";
    /**登录访问，无需授权*/
    String ROLE_SKIP="ROLE_SKIP";
}
