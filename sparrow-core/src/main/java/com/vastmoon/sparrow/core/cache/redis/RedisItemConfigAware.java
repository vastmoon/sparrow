package com.vastmoon.sparrow.core.cache.redis;

import com.vastmoon.sparrow.core.common.SparrowAware;

/**
 * redis item config 回调接口，动态注入配置项
 *
 * @author yousuf 2020/12/31 09:43
 **/
public interface RedisItemConfigAware extends SparrowAware<FlexibleRedisProperties.RedisItemConfig> {
}
