package com.vastmoon.sparrow.core.crypto.aes;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 配置文件
 *
 * @author yousuf 2020/12/31 11:16
 **/
@Data
public class AesProperties {
    /**aes名称*/
    @NotEmpty
    private String name;
    /**秘钥长度*/
    private Integer keySize = 256;
    /**aes key值*/
    private String key;
}
