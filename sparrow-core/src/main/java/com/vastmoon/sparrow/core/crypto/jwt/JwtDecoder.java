package com.vastmoon.sparrow.core.crypto.jwt;

/**
 * jwt解码器
 *
 * @author yousuf 2021/1/7 09:29
 **/
public interface JwtDecoder {
    /**
     * 解码jwt信息
     * 解码信息最好序列化为json格式
     * @param token 信息
     * @return 解码以后的token信息
     */
    String decode(String token);
}
