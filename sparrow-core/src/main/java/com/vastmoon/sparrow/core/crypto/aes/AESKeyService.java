package com.vastmoon.sparrow.core.crypto.aes;

/**
 * 这个方法主要用于双重加密使用
 * KEK 外层秘钥，一般为RSA提供
 * DEK 数据加密秘钥，一般通过数据库查询，然后通过KEK解密得到 DEK
 *
 * @author yousuf 2020/12/31 11:05
 **/
public interface AESKeyService {
    String AES_KEY_SERVICE_NAME = "AESKeyService";

    /**
     * 通过名称 找到对应的aes key
     * @param name key名称
     * @return key
     */
    AESStore findAESKey(String name);
}
