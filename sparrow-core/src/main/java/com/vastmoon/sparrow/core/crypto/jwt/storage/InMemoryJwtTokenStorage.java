package com.vastmoon.sparrow.core.crypto.jwt.storage;

import com.google.common.collect.Maps;
import com.vastmoon.sparrow.core.crypto.jwt.JwtTokenStorage;
import com.vastmoon.sparrow.core.crypto.jwt.TokenId;

import java.util.Map;

/**
 * 内存缓存
 *
 * @author yousuf 2021/1/8 11:03
 **/
public class InMemoryJwtTokenStorage implements JwtTokenStorage {
    private final Map<TokenId, String> cache = Maps.newHashMap();
    @Override
    public String put(TokenId tokenId, String token) {
        return cache.put(tokenId, token);
    }

    @Override
    public String get(TokenId tokenId) {
        return cache.get(tokenId);
    }

    @Override
    public void expire(TokenId tokenId) {
        cache.remove(tokenId);
    }
}
