package com.vastmoon.sparrow.core.exception.framework;

import com.vastmoon.sparrow.client.dto.rest.RestCode;
import lombok.Getter;

/**
 * 框架类异常
 *
 * @author yousuf 2020/12/23 09:09
 **/
public enum FrameworkCode implements RestCode {
    /**异常代码*/
    FRAMEWORK_ERROR("FRAMEWORK_ERROR", "Framework error"),
    BIZ_ERROR("BIZ_ERROR", "General business logic error"),
    SYS_ERROR("SYS_ERROR" , "Unknown system error" ),
    ILLEGAL_PARAM_ERROR("ILLEGAL_PARAM_ERROR", "Request parameter exception"),
    METHOD_NOT_ALLOWED("METHOD_NOT_ALLOWED", "Request method error"),
    JSON_CONVERT_ERROR("JSON_CONVERT_ERROR", "json format error"),
    NOT_FOUND("NOT_FOUND", "not found"),
    ;
    @Getter
    private final String code;
    @Getter
    private final String message;

    FrameworkCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
