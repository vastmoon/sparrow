package com.vastmoon.sparrow.core.security.dynamic;

import com.vastmoon.sparrow.client.dto.security.MetaResource;

import java.util.Set;

/**
 * 资源处理器
 *
 * @author yousuf 2021/1/7 16:03
 **/
public interface MetaResourceProcessor {
    /**
     * 查询所有权限
     * @return 权限集合
     */
    Set<MetaResource> queryPatternsAndMethods();
}
