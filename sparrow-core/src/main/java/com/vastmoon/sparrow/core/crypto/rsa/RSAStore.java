package com.vastmoon.sparrow.core.crypto.rsa;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.vastmoon.sparrow.core.crypto.rsa.util.RSAUtils;
import lombok.Getter;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * RSA 持有类，用于加密解密
 *
 * @author yousuf 2020/12/31 11:30
 **/
public class RSAStore {
    @Getter
    private final String name;
    private final RSA rsa;
    public RSAStore(RSAProperties properties) {
        this.rsa = RSAUtils.generateRsa(properties);
        this.name = properties.getName();

    }


    /**
     * 解密数据
     * @param data 密文
     * @return 明文
     */
    public String decrypt(String data) {
        return rsa.decryptStr(data, KeyType.PrivateKey);
    }

    /**
     * 数据加密
     * @param data 明文
     * @return 密文
     */
    public String encrypt(String data) {
        return rsa.encryptBase64(data, KeyType.PublicKey);
    }


    /**
     * 获取私钥
     * @return 私钥
     */
    public PrivateKey privateKey() {
        return rsa.getPrivateKey();
    }

    /**
     * 获取公钥
     * @return 公钥
     */
    public PublicKey publicKey() {
        return rsa.getPublicKey();
    }

    /**
     * 获取keypair
     * @return keypair
     */
    public KeyPair keyPair() {
        return new KeyPair(publicKey(), privateKey());
    }
}
