package com.vastmoon.sparrow.core.jpa;

import com.vastmoon.sparrow.core.domain.PersistenceObject;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * 设计字段的数据表
 *
 * @author yousuf 2020/12/31 11:47
 **/
@Getter
@Setter
@ToString
@MappedSuperclass
@RequiredArgsConstructor
@EntityListeners({AuditingEntityListener.class})
public abstract class BasePO implements PersistenceObject {
    private static final long serialVersionUID = 8345634293379897327L;
    @CreatedBy
    @Column(name = "gmt_create_by")
    protected String createBy;

    @CreatedDate
    @Column(name = "gmt_create_at", updatable = false)
    protected LocalDateTime createAt;
    @LastModifiedBy
    @Column(name = "gmt_modified_by")
    protected String modifiedBy;

    @LastModifiedDate
    @Column(name = "gmt_modified_at")
    protected LocalDateTime modifiedAt;

    @Column(name = "remark")
    protected String remark;
}
