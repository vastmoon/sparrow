package com.vastmoon.sparrow.core.crypto.jwt.nimbus;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.vastmoon.sparrow.core.crypto.jwt.JwtDecoder;
import com.vastmoon.sparrow.core.crypto.jwt.JwtEncoder;
import com.vastmoon.sparrow.core.crypto.jwt.exception.JwtException;
import com.vastmoon.sparrow.core.crypto.jwt.exception.JwtRestCode;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;

/**
 * Nimbus 实现
 *
 * @author yousuf 2021/1/7 09:49
 **/
@Slf4j
@RequiredArgsConstructor
public class NimbusJwtTokenGenerator implements JwtEncoder, JwtDecoder, InitializingBean {
    private static final String JWT_EXP_KEY = "exp";
    private final KeyPair keyPair;
    private JWSHeader jwsHeader;

    @SneakyThrows
    @Override
    public String encode(String token) {
        Payload payload = new Payload(token);
        JWSObject jwsObject = new JWSObject(jwsHeader, payload);
        JWSSigner jwsSigner = new RSASSASigner(this.keyPair.getPrivate(), true);
        jwsObject.sign(jwsSigner);
        return jwsObject.serialize();
    }

    @SneakyThrows
    @Override
    public String decode(String token) {
        Assert.hasText(token, "jwt token must not be bank");
        JWSObject jwsObject = JWSObject.parse(token);
        RSAPublicKey publicKey = (RSAPublicKey) this.keyPair.getPublic();
        JWSVerifier jwsVerifier = new RSASSAVerifier(publicKey);
        if (!jwsObject.verify(jwsVerifier)) {
            throw new JwtException(JwtRestCode.TOKEN_SING_ERROR, "token签名不合法！");
        }
        JSONObject jsonObject = jwsObject.getPayload().toJSONObject();
        String exp = jsonObject.getAsString(JWT_EXP_KEY);
        if (isExpired(exp)) {
            throw new JwtException(JwtRestCode.TOKEN_EXPIRED,"jwt token is expired");
        }
        return jsonObject.toJSONString();
    }

    private boolean isExpired(String exp) {
        return LocalDateTime.now().isAfter(LocalDateTime.now());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
                .type(JOSEObjectType.JWT)
                .build();
    }
}
