package com.vastmoon.sparrow.core.exception.framework;

import com.vastmoon.sparrow.client.dto.rest.RestCode;
import com.vastmoon.sparrow.core.exception.framework.BaseException;

/**
 * 框架类异常
 *
 * @author yousuf 2020/12/22 17:57
 **/
public class FrameworkException extends BaseException {
    private static final long serialVersionUID = 271958824889646670L;

    public FrameworkException(String errorMessage) {
        super(errorMessage);
        this.setCode(FrameworkCode.FRAMEWORK_ERROR);
    }

    public FrameworkException(String errorMessage, Throwable e) {
        super(errorMessage, e);
        this.setCode(FrameworkCode.FRAMEWORK_ERROR);
    }

    public FrameworkException(RestCode code, String errorMessage) {
        super(errorMessage);
        this.setCode(code);
    }
}
