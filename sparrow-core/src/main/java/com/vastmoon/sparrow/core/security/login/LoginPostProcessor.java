package com.vastmoon.sparrow.core.security.login;

import javax.servlet.ServletRequest;

/**
 * 登录处理器
 *
 * @author yousuf 2021/1/5 11:43
 **/
public interface LoginPostProcessor {
    String LOGIN_PROCESSING_URL ="/login-process";
    /**
     * 获取登录方式
     * @return 登录方式
     */
    String getLoginType();

    /**
     * 获取用户名
     * @param request request
     * @return 用户名
     */
    String obtainUsername(ServletRequest request);

    /**
     * 获取密码
     * @param request request
     * @return 密码
     */
    String obtainPassword(ServletRequest request);

}
