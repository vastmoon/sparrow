package com.vastmoon.sparrow.core.crypto.rsa.util;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.AsymmetricAlgorithm;
import cn.hutool.crypto.asymmetric.RSA;
import com.vastmoon.sparrow.core.crypto.rsa.RSAProperties;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.DefaultResourceLoader;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Objects;

/**
 * RSA 辅助操作类
 *
 * @author yousuf 2020/12/31 11:33
 **/
@UtilityClass
public class RSAUtils {
    public RSA generateRsa(RSAProperties properties) {
        AsymmetricCryptography cryptography = new AsymmetricCryptography(new DefaultResourceLoader());
        PrivateKey privateKey = null;
        PublicKey publicKey = null;
        if (StringUtils.isNotBlank(properties.getPrivateKeyPath())) {
            privateKey = cryptography.getPrivateKey(properties.getPrivateKeyPath(), properties.getKeyFormat());
        }
        if (StringUtils.isNotBlank(properties.getPublicKeyPath())) {
            publicKey = cryptography.getPublicKey(properties.getPublicKeyPath(), properties.getKeyFormat());
        }
        byte[] privateKeyEncoded = Objects.isNull(privateKey) ? null : privateKey.getEncoded();
        byte[] publicKeyEncoded = Objects.isNull(publicKey) ? null : publicKey.getEncoded();
        return SecureUtil.rsa(privateKeyEncoded, publicKeyEncoded);
    }

    /**
     * 生成秘钥
     * @param properties 配置
     * @return 秘钥
     */
    public KeyPair generateKeyPair(RSAProperties properties) {
        return SecureUtil.generateKeyPair(AsymmetricAlgorithm.RSA_ECB_PKCS1.getValue(), properties.getKeySize());
    }

}
