package com.vastmoon.sparrow.core.crypto.jwt;

import lombok.Getter;

/**
 * refreshToken
 *
 * @author yousuf 2021/1/7 11:07
 **/
@Getter
public class RefreshTokenId implements TokenId {
    private static final long serialVersionUID = -4165889130677891279L;
    private static final String DEFAULT_PREFIX= "refresh_token_";

    private final String prefix;
    private final String aud;

    public RefreshTokenId(String aud) {
        this.aud = aud;
        this.prefix = DEFAULT_PREFIX;
    }

    public RefreshTokenId(String prefix, String aud) {
        this.aud = aud;
        this.prefix = prefix;
    }

    @Override
    public String toString() {
        return this.prefix + this.aud;
    }
}
