package com.vastmoon.sparrow.core.security.filter;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.vastmoon.sparrow.core.crypto.jwt.AccessTokenId;
import com.vastmoon.sparrow.core.crypto.jwt.JwtDecoder;
import com.vastmoon.sparrow.core.crypto.jwt.JwtTokenStorage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * jwt 过滤器
 *
 * @author yousuf 2021/1/7 14:06
 **/
@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private static final String AUTHENTICATION_PREFIX = "Bearer ";
    private final JwtDecoder jwtDecoder;
    private JwtTokenStorage accessTokenStorage;

    @Override
    @SuppressWarnings("NullableProblems")
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // 如果已经通过认证
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            filterChain.doFilter(request, response);
            return;
        }

        // 获取 header 解析出 jwt 并进行认证 无token 直接进入下一个过滤器  因为  SecurityContext 的缘故 如果无权限并不会放行
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.hasText(header) && header.startsWith(AUTHENTICATION_PREFIX)) {
            String jwtToken = header.replace(AUTHENTICATION_PREFIX, "");
            if (StringUtils.hasText(jwtToken)) {
                try {
                    authenticationTokenHandle(jwtToken, request);
                } catch (Exception ignored) { }
            }
        }
        filterChain.doFilter(request, response);
    }

    private void authenticationTokenHandle(String jwtToken, HttpServletRequest request) {
        JSONObject jsonObject = null;
        try {
            String payload = jwtDecoder.decode(jwtToken);
            jsonObject = JSONUtil.parseObj(payload);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("token : {}  is  invalid", jwtToken);
            }
            throw new BadCredentialsException("token is invalid");
        }
        String username = jsonObject.getStr("aud");

        // 从缓存获取 token
        String accessToken = accessTokenStorage.get(new AccessTokenId(username));
        if (StringUtils.isEmpty(accessToken)) {
            if (log.isDebugEnabled()) {
                log.debug("token : {}  is  not in cache", accessToken);
            }
            // 缓存中不存在就算 失败了
            throw new CredentialsExpiredException("token is not in cache");
        }

        if (jwtToken.equals(accessToken)) {
            // 解析 权限集合  这里
            JSONArray jsonArray = jsonObject.getJSONArray("roles");
            List<String> roles = jsonArray.toList(String.class);
            String[] roleArr = roles.toArray(new String[0]);

            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(roleArr);
            User user = new User(username, "[PROTECTED]", authorities);
            // 构建用户认证token
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(user, null, authorities);
            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            // 放入安全上下文中
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        } else {
            // token 不匹配
            if (log.isDebugEnabled()){
                log.debug("token : {}  is  not in matched", jwtToken);
            }

            throw new BadCredentialsException("token is not matched");
        }
    }

    @Autowired
    @Qualifier(JwtTokenStorage.ACCESS_JWT_TOKEN_STORAGE_NAME)
    public void setAccessTokenStorage(JwtTokenStorage jwtTokenStorage) {
        this.accessTokenStorage = jwtTokenStorage;
    }
}
