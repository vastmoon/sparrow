package com.vastmoon.sparrow.core.security.handler;

import cn.hutool.core.collection.CollectionUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vastmoon.sparrow.client.dto.rest.RestBody;
import com.vastmoon.sparrow.client.dto.rest.RestMap;
import com.vastmoon.sparrow.client.dto.rest.RestMapFactory;
import com.vastmoon.sparrow.core.crypto.jwt.*;
import com.vastmoon.sparrow.core.util.WebUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * 认证成功返回信息
 *
 * @author yousuf 2021/1/6 17:21
 **/
@Slf4j
@RequiredArgsConstructor
public class CustomizeJwtAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private final JwtEncoder jwtEncoder;
    private final PayloadGenerator payloadGenerator;
    private final ObjectMapper objectMapper;
    private JwtTokenStorage accessTokenStorage;
    private JwtTokenStorage refreshTokenStorage;
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (response.isCommitted()) {
            log.debug("Response has already been committed");
            return;
        }

        User principal = (User) authentication.getPrincipal();
        String username = principal.getUsername();
        Collection<GrantedAuthority> authorities = principal.getAuthorities();
        Set<String> roles = new HashSet<>();
        if (CollectionUtil.isNotEmpty(authorities)) {
            for (GrantedAuthority authority : authorities) {
                String roleName = authority.getAuthority();
                roles.add(roleName);
            }
        }
        TokenId accessTokenId = new AccessTokenId(username);
        TokenId refreshTokenId = new RefreshTokenId(username);
        Map<String, Object> result = new HashMap<>(2);
        String accessToken = jwtEncoder.encode(payloadGenerator.accessPayload(accessTokenId, roles, null));
        String refreshToken = jwtEncoder.encode(payloadGenerator.refreshPayload(refreshTokenId, roles, null));
        accessTokenStorage.put(accessTokenId, accessToken);
        refreshTokenStorage.put(refreshTokenId, refreshToken);
        result.put("access_token", accessToken);
        result.put("refresh_token", refreshToken);
        RestMap restMap = RestMapFactory.ofData(RestBody.ok(), result);
        WebUtils.responseJsonWriter(response, objectMapper.writeValueAsString(restMap.toMap()), HttpServletResponse.SC_OK);
    }


    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Qualifier(JwtTokenStorage.ACCESS_JWT_TOKEN_STORAGE_NAME)
    public void setAccessTokenStorage(JwtTokenStorage accessTokenStorage) {
        this.accessTokenStorage = accessTokenStorage;
    }

    @Autowired
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Qualifier(JwtTokenStorage.REFRESH_JWT_TOKEN_STORAGE_NAME)
    public void setRefreshTokenStorage(JwtTokenStorage refreshTokenStorage) {
        this.refreshTokenStorage = refreshTokenStorage;
    }
}
