package com.vastmoon.sparrow.core.exception;

import com.vastmoon.sparrow.client.dto.rest.RestCode;
import com.vastmoon.sparrow.core.exception.framework.BaseException;
import com.vastmoon.sparrow.core.exception.framework.FrameworkCode;

/**
 * 系统异常
 *
 * @author yousuf 2020/12/23 09:18
 **/
public class SysException extends BaseException {
    private static final long serialVersionUID = 7751575302593235589L;

    public SysException(String errorMessage) {
        super(errorMessage);
        this.setCode(FrameworkCode.SYS_ERROR);
    }

    public SysException(String errorMessage, Throwable e) {
        super(errorMessage, e);
        this.setCode(FrameworkCode.SYS_ERROR);
    }

    public SysException(RestCode code, String errorMessage) {
        super(errorMessage);
        this.setCode(code);
    }
}
