package com.vastmoon.sparrow.core.crypto.jwt;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * accessToken
 *
 * @author yousuf 2021/1/7 10:54
 **/
@Getter
@EqualsAndHashCode
public class AccessTokenId implements TokenId {
    private static final long serialVersionUID = 7167957332932905160L;
    private static final String DEFAULT_PREFIX= "access_token_";

    private final String prefix;
    private final String aud;

    public AccessTokenId(String aud) {
        this.aud = aud;
        this.prefix = DEFAULT_PREFIX;
    }

    public AccessTokenId(String prefix, String aud) {
        this.aud = aud;
        this.prefix = prefix;
    }

    @Override
    public String toString() {
        return this.prefix + this.aud;
    }
}
