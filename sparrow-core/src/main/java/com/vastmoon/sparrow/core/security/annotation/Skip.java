package com.vastmoon.sparrow.core.security.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 跳过权限校验
 *
 * @author yousuf 2021/1/19 13:10
 **/
@Target({ ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Skip {
    /**
     * 通过该注解来控制两个角色是否有效
     * true 登录可以访问 {@link com.vastmoon.sparrow.core.security.SecurityConstants#ROLE_SKIP}
     * false 匿名可以访问 {@link com.vastmoon.sparrow.core.security.SecurityConstants#ROLE_ANONYMOUS}
     * @return 是否匿名登录
     * */
    boolean logged() default true;

}
