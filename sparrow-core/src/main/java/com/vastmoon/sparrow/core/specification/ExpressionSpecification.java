package com.vastmoon.sparrow.core.specification;

import java.util.Objects;
import java.util.function.Function;

/**
 * 把规约模式封装为函数式
 *
 * @author yousuf 2020/12/25 16:58
 **/
public class ExpressionSpecification<T> extends AbstractCompositeSpecification<T> {
    private final Function<T, Boolean> expression;

    public ExpressionSpecification(Function<T, Boolean> expression) {
        if (Objects.isNull(expression)) {
            throw new IllegalArgumentException("expression not null");
        }
        this.expression = expression;
    }

    @Override
    public boolean isSatisfiedBy(T candidate) {
        return this.expression.apply(candidate);
    }
}
