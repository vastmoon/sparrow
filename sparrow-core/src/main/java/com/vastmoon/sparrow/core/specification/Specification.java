package com.vastmoon.sparrow.core.specification;

/**
 * 规约模式 一般用于校验
 *
 * @author yousuf 2020/12/25 16:32
 **/
public interface Specification<T> {
    /**
     * 规则验证接口
     * @param candidate 校验对象
     *
     * @return 是否通过
     */
    default boolean isSatisfiedBy(T candidate){return true;}

    /**
     * 并且关系
     * @param specification 规则
     * @return 规则类
     */
    Specification<T> and(Specification<T> specification);

    /**
     * 或关系 ||
     * @param specification 规则
     *
     * @return 规则类
     */
    Specification<T> or(Specification<T> specification);

    /**
     * 不等于关系
     * @param specification 规则
     * @return 规则类
     */
    Specification<T> not(Specification<T> specification);
}
