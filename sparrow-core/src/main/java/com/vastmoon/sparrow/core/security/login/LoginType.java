package com.vastmoon.sparrow.core.security.login;

import java.lang.annotation.*;

/**
 * 登录方式注解
 *
 * @author yousuf 2021/1/5 11:33
 **/
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginType {
    String type();

    boolean enabled() default true;
}
