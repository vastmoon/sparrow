package com.vastmoon.sparrow.core.crypto.jwt;

/**
 * jwt token 缓存
 *
 * @author yousuf 2021/1/6 15:53
 **/
public interface JwtTokenStorage {
    String ACCESS_JWT_TOKEN_STORAGE_NAME = "accessJwtTokenStorage";
    String REFRESH_JWT_TOKEN_STORAGE_NAME = "refreshJwtTokenStorage";
    /**
     * 放入缓存
     * @param tokenId tokenId
     * @param token token
     * @return tokenPair
     */
    String put(TokenId tokenId, String token);

    /**
     * 从缓存中获取
     * @param tokenId tokenId
     * @return tokenPair
     */
    String get(TokenId tokenId);

    /**
     * 清除缓存
     * @param tokenId tokenId
     */
    void expire(TokenId tokenId);
}
