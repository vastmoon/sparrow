package com.vastmoon.sparrow.core.crypto.rsa;

import com.vastmoon.sparrow.core.crypto.rsa.util.AsymmetricCryptography;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * rsa 配置类
 *
 * @author yousuf 2020/12/31 11:29
 **/
@Data
public class RSAProperties {
    @NotEmpty
    private String name;
    private String privateKeyPath;
    private String publicKeyPath;
    private AsymmetricCryptography.KeyFormat keyFormat = AsymmetricCryptography.KeyFormat.DER;
    /**秘钥长度*/
    private Integer keySize = 2048;
}
