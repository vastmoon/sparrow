package com.vastmoon.sparrow.core.common;

/**
 * 用于spring动态加载类的标志
 *
 * @author yousuf 2020/12/28 15:07
 **/
public interface SparrowAware<T> {
    /**
     * 获取接口类型的实例
     * @return 实例
     */
    T instantiate();
}
