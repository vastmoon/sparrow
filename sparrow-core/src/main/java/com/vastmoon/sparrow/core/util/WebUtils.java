package com.vastmoon.sparrow.core.util;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * WebUtils 工具类封装
 *
 * @author yousuf 2020/12/28 11:57
 **/
@UtilityClass
public class WebUtils extends org.springframework.web.util.WebUtils {
    /**IP头文件*/
    private static final String[] IP_HEADERS = {
            "X-Forwarded-For",
            "X-Real-IP",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP"
    };

    /**
     * 获取request
     * @return request
     */
    public HttpServletRequest getRequest() {
        ServletRequestAttributes attrs =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        RequestContextHolder.setRequestAttributes(attrs, true);
        return Objects.requireNonNull(attrs).getRequest();
    }

    /**
     * 返回json格式数据
     * @param response response
     * @param json json字符串
     * @param status http 状态码
     * @throws IOException 异常信息
     */
    public void responseJsonWriter(HttpServletResponse response, String json, int status) throws IOException {
        response.setStatus(status);
        response.setCharacterEncoding("utf-8");
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getOutputStream().write(json.getBytes());
    }

    /**
     * 获取请求的ip地址
     * @return 请求ip地址
     */
    public String getIpAddress() {
        for (String ipHeader : IP_HEADERS) {
            String ip = getRequest().getHeader(ipHeader);
            if (!StringUtils.isEmpty(ip) && !ip.contains("unknown")) {
                return ip;
            }
        }
        return getRequest().getRemoteAddr();
    }

    /**
     * 获取 请求全路径
     * @param request request
     * @return 请求路径
     */
    public String requestFullUrl(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getRequestURI();
    }

    /**
     * 获取 请求全路径
     * @return 请求路路径
     */
    public String requestFullUrl() {
        return requestFullUrl(getRequest());
    }
}
