package com.vastmoon.sparrow.core.domain;

import java.io.Serializable;

/**
 * 持久化对象接口
 *
 * @author yousuf 2020/12/25 14:30
 **/
public interface PersistenceObject extends Serializable {
}

