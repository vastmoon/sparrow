package com.vastmoon.sparrow.core.security.login;

import javax.servlet.ServletRequest;

import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY;
import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY;

/**
 * 默认登录方式
 *
 * @author yousuf 2021/1/5 12:01
 **/
public class FormLoginPostProcessor implements LoginPostProcessor {
    public static final String LOGIN_TYPE= "form";
    @Override
    public String getLoginType() {
        return LOGIN_TYPE;
    }

    @Override
    public String obtainUsername(ServletRequest request) {
        return request.getParameter(SPRING_SECURITY_FORM_USERNAME_KEY);
    }

    @Override
    public String obtainPassword(ServletRequest request) {
        return request.getParameter(SPRING_SECURITY_FORM_PASSWORD_KEY);
    }
}
