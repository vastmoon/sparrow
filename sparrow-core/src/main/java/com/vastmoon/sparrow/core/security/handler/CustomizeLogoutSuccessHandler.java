package com.vastmoon.sparrow.core.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vastmoon.sparrow.client.dto.rest.RestBody;
import com.vastmoon.sparrow.core.util.WebUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 退出成功
 *
 * @author yousuf 2021/1/5 14:36
 **/
@Slf4j
@RequiredArgsConstructor
public class CustomizeLogoutSuccessHandler implements LogoutSuccessHandler {
    private final ObjectMapper objectMapper;
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        User user = (User) authentication.getPrincipal();
        String username = user.getUsername();
        log.info("username: {}  is offline now", username);
        WebUtils.responseJsonWriter(response, objectMapper.writeValueAsString(RestBody.ok("退出成功")), HttpServletResponse.SC_OK);
    }
}
