package com.vastmoon.sparrow.core.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vastmoon.sparrow.client.dto.rest.RestBody;
import com.vastmoon.sparrow.client.dto.rest.RestMap;
import com.vastmoon.sparrow.client.dto.rest.RestMapFactory;
import com.vastmoon.sparrow.core.security.CustomizeSecurityCode;
import com.vastmoon.sparrow.core.security.util.AuthenticationExceptionUtils;
import com.vastmoon.sparrow.core.util.WebUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 认证失败
 *
 * @author yousuf 2021/1/6 17:51
 **/
@Slf4j
@RequiredArgsConstructor
public class CustomizeAuthenticationFailureHandler implements AuthenticationFailureHandler {
    private final ObjectMapper objectMapper;
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        Map<String, Object> errors = AuthenticationExceptionUtils.ofAuthenticationException(exception.getClass());
        RestMap restMap = RestMapFactory.ofErrors(RestBody.failure(CustomizeSecurityCode.UNAUTHORIZED), errors);
        WebUtils.responseJsonWriter(response, objectMapper.writeValueAsString(restMap.toMap()), HttpServletResponse.SC_UNAUTHORIZED);
    }
}
