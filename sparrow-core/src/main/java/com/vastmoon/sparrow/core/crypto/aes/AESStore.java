package com.vastmoon.sparrow.core.crypto.aes;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import lombok.Getter;

/**
 * aes store 用于ase加密解密
 *
 * @author yousuf 2020/12/31 11:18
 **/
public class AESStore {
    @Getter
    private final String name;
    private final AES aes;

    public AESStore(String name, String aesKey) {
        this.name = name;
        aes =  SecureUtil.aes(Base64.decode(aesKey));
    }

    /**
     * <p> Title: encrypt
     * <p> Description: aes 加密数据
     *
     * @param data 明文
     *
     * @return java.lang.String 密文
     *
     * @author yousuf 2020/2/27
     *
     */
    public String encrypt(String data) {
        return aes.encryptBase64(data);
    }

    /**
     * <p> Title: decrypt
     * <p> Description: 解密数据
     *
     * @param data 密文
     *
     * @return java.lang.String 明文
     *
     * @author yousuf 2020/2/27
     *
     */
    public String decrypt(String data) {
        return aes.decryptStr(data);
    }
}
