package com.vastmoon.sparrow.core.specification;

import lombok.RequiredArgsConstructor;

/**
 * 且关系实现类
 *
 * @author yousuf 2020/12/25 16:47
 **/
@RequiredArgsConstructor
public class AndSpecification<T> extends AbstractCompositeSpecification<T> {
    private final Specification<T> left;
    private final Specification<T> right;

    @Override
    public boolean isSatisfiedBy(T candidate) {
        return left.isSatisfiedBy(candidate) && right.isSatisfiedBy(candidate);
    }
}
