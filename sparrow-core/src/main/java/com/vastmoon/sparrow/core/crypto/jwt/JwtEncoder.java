package com.vastmoon.sparrow.core.crypto.jwt;

/**
 * jwt编码器
 *
 * @author yousuf 2021/1/7 09:40
 **/
public interface JwtEncoder {
    /**
     * jwt编码
     * @param token 编码前的信息
     * @return jwt token
     */
    String encode(String token);
}
