package com.vastmoon.sparrow.core.common.annotationmap;

import com.google.common.collect.Maps;
import com.vastmoon.sparrow.core.common.AbstractAnnotationBeanMap;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 支付方式持有类
 *
 * @author yousuf 2020/12/25 12:16
 **/
@Component
public class PaymentHandler extends AbstractAnnotationBeanMap<Pay, Payment> {
    private final Map<PaymentMethod, Payment> payments = Maps.newHashMap();
    @Override
    protected Class<Pay> getAnnotation() {
        return Pay.class;
    }

    @Override
    protected void annotationBeanMap(Map<Pay, Payment> annotationBeanMap) {
        annotationBeanMap.forEach((pay, payment) -> payments.put(pay.method(), payment));
    }

    /**
     * 执行支付方式
     * @param method 支付方式
     * @return 支付结果
     */
    public String pay(PaymentMethod method) {
        return payments.get(method).pay();
    }
}
