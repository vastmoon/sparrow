package com.vastmoon.sparrow.core.common.annotationmap;

/**
 * 支付接口
 *
 * @author yousuf 2020/12/25 12:11
 **/
public interface Payment {
    String pay();
}
