package com.vastmoon.sparrow.core.common.annotationmap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * google pay
 *
 * @author yousuf 2020/12/25 12:14
 **/
@Component
@Pay(method = PaymentMethod.GOOGLE_PAY)
public class GooglePay implements Payment {
    Logger log = LoggerFactory.getLogger(AliPay.class);
    @Override
    public String pay() {
        log.info("Google Pay 支付完成");
        return "Google Pay 支付完成";
    }
}
