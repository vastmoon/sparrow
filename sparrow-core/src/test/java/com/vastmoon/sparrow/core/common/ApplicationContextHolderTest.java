package com.vastmoon.sparrow.core.common;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 测试文件
 *
 * @author yousuf 2020/12/25 10:54
 **/
@SpringBootTest(classes = {ApplicationContextHolder.class})
class ApplicationContextHolderTest {
    @Autowired
    private ApplicationContextHolder applicationContextHolder;

    @Test
    void when_test_txt_then_return_file_name_text_txt() {
        assertNotNull(ApplicationContextHolder.get());
        Resource resource = ApplicationContextHolder.getResource(":classpath/text.txt");
        assertEquals(resource.getFilename(), "text.txt");
    }
}