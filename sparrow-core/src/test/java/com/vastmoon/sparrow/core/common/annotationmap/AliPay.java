package com.vastmoon.sparrow.core.common.annotationmap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * alipay支付
 *
 * @author yousuf 2020/12/25 12:11
 **/
@Component
@Pay(method = PaymentMethod.ALIPAY)
public class AliPay implements Payment {
    Logger log = LoggerFactory.getLogger(AliPay.class);
    @Override
    public String pay() {
        log.info("alipay 支付完成");
        return "alipay 支付完成";
    }
}
