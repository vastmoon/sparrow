package com.vastmoon.sparrow.core.common.annotationmap;

import java.lang.annotation.*;

/**
 * 支付注解
 *
 * @author yousuf 2020/12/25 12:03
 **/
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Pay {
    PaymentMethod method();
}
