package com.vastmoon.sparrow.core.common;

import org.springframework.context.annotation.ComponentScan;

/**
 * 测试配置文件
 *
 * @author yousuf 2020/12/25 12:09
 **/
@ComponentScan("com.vastmoon.sparrow.core")
public class CoreConfig {
}
