package com.vastmoon.sparrow.core.common;

import com.vastmoon.sparrow.core.common.annotationmap.PaymentHandler;
import com.vastmoon.sparrow.core.common.annotationmap.PaymentMethod;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 测试AnnotationBeanMap
 *
 * @author yousuf 2020/12/25 12:02
 **/
@SpringBootTest(classes = CoreConfig.class)
class AbstractAnnotationBeanMapTest {
    @Autowired
    private PaymentHandler paymentHandler;

    @Test
    void when_payment_method_alipay_return_do_alipay() {
        String message = paymentHandler.pay(PaymentMethod.ALIPAY);
        assertEquals(message, "alipay 支付完成");
    }
}