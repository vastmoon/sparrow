package com.vastmoon.sparrow.core.specification;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 规约模式测试类
 *
 * @author yousuf 2020/12/25 17:00
 **/
class ExpressionSpecificationTest {

    @Test
    void when_new_and_then_return_true() {
        boolean verify = new ExpressionSpecification<String>(s -> s.startsWith("aaa"))
                .and(new ExpressionSpecification<>(s -> s.contains("test")))
                .isSatisfiedBy("aaa_test_abss");
        assertTrue(verify);
    }

    @Test
    void when_new_or_then_return_true() {
        boolean verify = new ExpressionSpecification<String>(s -> s.startsWith("aaa"))
                .or(new ExpressionSpecification<>(s -> s.contains("or")))
                .isSatisfiedBy("aaa_test_abss");
        assertTrue(verify);
    }

    @Test
    void when_new_not_then_return_true() {
        boolean verify = new ExpressionSpecification<String>(s -> s.startsWith("and"))
                .not(new ExpressionSpecification<>(s -> s.contains("or")))
                .isSatisfiedBy("aaa_test_abss");
        assertTrue(verify);
    }

}