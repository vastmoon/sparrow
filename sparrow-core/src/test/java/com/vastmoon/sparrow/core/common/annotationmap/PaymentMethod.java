package com.vastmoon.sparrow.core.common.annotationmap;

/**
 * 测试支付方式
 *
 * @author yousuf 2020/12/25 12:07
 **/
public enum PaymentMethod {
    WECHAT_PAY, GOOGLE_PAY, ALIPAY;
}
