package com.vastmoon.sparrow.autoconfigure.crypto;

import com.google.common.collect.Sets;
import com.vastmoon.sparrow.core.crypto.aes.AesProperties;
import com.vastmoon.sparrow.core.crypto.jwt.JwtProperties;
import com.vastmoon.sparrow.core.crypto.rsa.RSAProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Set;

/**
 * 配置参数类
 *
 * @author yousuf 2020/12/31 11:03
 **/
@Data
@Validated
@ConfigurationProperties(prefix = "vastmoon.crypto")
public class CryptoProperties {
    @Valid
    @NestedConfigurationProperty
    private Set<RSAProperties> rsa = Sets.newHashSet();
    @Valid
    @NestedConfigurationProperty
    private Set<AesProperties> aes = Sets.newHashSet();
    @NestedConfigurationProperty
    private JwtProperties jwt = new JwtProperties();
}
