package com.vastmoon.sparrow.autoconfigure.crypto.jwt;

import com.nimbusds.jose.JWSHeader;
import com.vastmoon.sparrow.autoconfigure.crypto.CryptoProperties;
import com.vastmoon.sparrow.core.crypto.jwt.JwtTokenStorage;
import com.vastmoon.sparrow.core.crypto.jwt.PayloadGenerator;
import com.vastmoon.sparrow.core.crypto.jwt.exception.JwtException;
import com.vastmoon.sparrow.core.crypto.jwt.exception.JwtRestCode;
import com.vastmoon.sparrow.core.crypto.jwt.nimbus.NimbusJwtTokenGenerator;
import com.vastmoon.sparrow.core.crypto.jwt.storage.InMemoryJwtTokenStorage;
import com.vastmoon.sparrow.core.crypto.rsa.RSAManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.KeyPair;

/**
 * jwt 配置信息
 *
 * @author yousuf 2021/1/7 11:12
 **/
@Slf4j
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(CryptoProperties.class)
@AutoConfigureAfter(CryptoProperties.class)
@ConditionalOnProperty(prefix = "vastmoon.crypto.jwt", name = "enabled", havingValue = "true")
public class JwtAutoConfiguration {

    @ConditionalOnMissingBean(name = JwtTokenStorage.ACCESS_JWT_TOKEN_STORAGE_NAME)
    @Bean(JwtTokenStorage.ACCESS_JWT_TOKEN_STORAGE_NAME)
    public JwtTokenStorage accessTokenStorage() {
        log.info("init default accessJwtTokenStorage");
        return new InMemoryJwtTokenStorage();
    }

    @ConditionalOnMissingBean(name = JwtTokenStorage.REFRESH_JWT_TOKEN_STORAGE_NAME)
    @Bean(JwtTokenStorage.REFRESH_JWT_TOKEN_STORAGE_NAME)
    public JwtTokenStorage referenceTokenStorage() {
        log.info("init default referenceTokenStorage");
        return new InMemoryJwtTokenStorage();
    }

    @Bean
    @ConditionalOnMissingBean
    public PayloadGenerator payloadGenerator(CryptoProperties properties) {
        log.info("init PayloadGenerator");
        return new PayloadGenerator(properties.getJwt());
    }

    @Bean
    @ConditionalOnClass(JWSHeader.class)
    @ConditionalOnMissingBean
    public NimbusJwtTokenGenerator jwtTokenGenerator(RSAManager rsaManager, CryptoProperties properties) {

        KeyPair keyPair = rsaManager.get(properties.getJwt().getJwtRsaName())
                .orElseThrow(() -> new JwtException(JwtRestCode.RSA_NAME_NOT_FIND, "RSA 秘钥初始化失败"))
                .keyPair();
        log.info("init NimbusJwtTokenGenerator");
        return new NimbusJwtTokenGenerator(keyPair);
    }
}
