package com.vastmoon.sparrow.autoconfigure.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vastmoon.sparrow.autoconfigure.crypto.jwt.JwtAutoConfiguration;
import com.vastmoon.sparrow.core.crypto.jwt.JwtDecoder;
import com.vastmoon.sparrow.core.crypto.jwt.JwtEncoder;
import com.vastmoon.sparrow.core.crypto.jwt.PayloadGenerator;
import com.vastmoon.sparrow.core.security.filter.JwtAuthenticationFilter;
import com.vastmoon.sparrow.core.security.filter.PreLoginFilter;
import com.vastmoon.sparrow.core.security.handler.*;
import com.vastmoon.sparrow.core.security.login.LoginPostProcessor;
import com.vastmoon.sparrow.core.security.login.LoginPostProcessorManger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * 自定义配置
 *
 * @author yousuf 2021/1/5 14:02
 **/
@Slf4j
@Import(DynamicSecurityConfiguration.class)
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(WebSecurityConfigurerAdapter.class)
@AutoConfigureAfter(JwtAutoConfiguration.class)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class CustomizeWebSecurityAutoConfiguration {

    @Bean
    public LoginPostProcessorManger loginPostProcessorManger() {
        log.info("init LoginPostProcessorManger");
        return new LoginPostProcessorManger();
    }

    @Bean
    public PreLoginFilter preLoginFilter(LoginPostProcessorManger postProcessorManger){
        log.info("init PreLoginFilter");
        return new PreLoginFilter(LoginPostProcessor.LOGIN_PROCESSING_URL, postProcessorManger);
    }

    @Bean
    @ConditionalOnMissingBean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    @ConditionalOnMissingBean
    public AuthenticationFailureHandler authenticationFailureHandler(ObjectMapper objectMapper) {
        return new CustomizeAuthenticationFailureHandler(objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean
    public LogoutSuccessHandler logoutSuccessHandler(ObjectMapper objectMapper) {
        return new CustomizeLogoutSuccessHandler(objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean
    public AccessDeniedHandler accessDeniedHandler(ObjectMapper objectMapper) {
        return new CustomizeAccessDeniedHandler(objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean
    public AuthenticationEntryPoint authenticationEntryPoint(ObjectMapper objectMapper) {
        return new CustomizeAuthenticationEntryPoint(objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean(value = {JwtEncoder.class, PayloadGenerator.class})
    public AuthenticationSuccessHandler authenticationSuccessHandler(JwtEncoder jwtEncoder,
                                                                     PayloadGenerator payloadGenerator,
                                                                     ObjectMapper objectMapper) {
        log.info("init CustomizeJwtAuthenticationSuccessHandler");
        return new CustomizeJwtAuthenticationSuccessHandler(jwtEncoder, payloadGenerator, objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean(value = {JwtEncoder.class})
    public JwtAuthenticationFilter jwtAuthenticationFilter(JwtDecoder jwtDecoder) {
        return new JwtAuthenticationFilter(jwtDecoder);
    }
}
