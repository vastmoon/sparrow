package com.vastmoon.sparrow.autoconfigure.cache;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 缓存自动配置类
 *
 * @author yousuf 2021/1/4 09:44
 **/
@Configuration(proxyBeanMethods = false)
@Import(value = {SparrowRedisCacheConfiguration.class, SparrowCaffeineCacheConfiguration.class})
public class CacheAutoConfiguration {
}
