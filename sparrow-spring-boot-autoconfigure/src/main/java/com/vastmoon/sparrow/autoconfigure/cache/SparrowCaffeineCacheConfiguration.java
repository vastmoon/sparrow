package com.vastmoon.sparrow.autoconfigure.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.vastmoon.sparrow.core.cache.caffeine.FlexibleCaffeineCacheManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Caffeine cache配置文件
 *
 * @author yousuf 2020/12/31 09:19
 **/
@Slf4j
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(value = Caffeine.class)
@EnableConfigurationProperties(CacheProperties.class)
public class SparrowCaffeineCacheConfiguration {
    @Bean(CacheProperties.CAFFEINE_CACHE_MANAGER)
    public FlexibleCaffeineCacheManager caffeineCacheManager(CacheProperties cacheProperties) throws Exception {
        log.info("Init FlexibleCaffeineCacheManager");
        return new FlexibleCaffeineCacheManager(cacheProperties.getCaffeine());
    }
}
