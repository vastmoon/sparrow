package com.vastmoon.sparrow.autoconfigure.cache;

import com.vastmoon.sparrow.core.cache.caffeine.CaffeineProperties;
import com.vastmoon.sparrow.core.cache.redis.FlexibleRedisProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * cache配置文件
 *
 * @author yousuf 2020/12/31 09:17
 **/
@Data
@ConfigurationProperties(prefix = "vastmoon.cache")
public class CacheProperties {
    public static final String CAFFEINE_CACHE_MANAGER = "caffeineCacheManager";
    public static final String REDIS_CACHE_MANAGER = "redisCacheManager";
    @NestedConfigurationProperty
    private CaffeineProperties caffeine = new CaffeineProperties();
    @NestedConfigurationProperty
    private FlexibleRedisProperties redis = new FlexibleRedisProperties();
}
