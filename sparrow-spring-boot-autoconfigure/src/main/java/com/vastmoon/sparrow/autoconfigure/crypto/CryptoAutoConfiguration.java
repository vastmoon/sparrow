package com.vastmoon.sparrow.autoconfigure.crypto;

import com.vastmoon.sparrow.autoconfigure.crypto.jwt.JwtAutoConfiguration;
import com.vastmoon.sparrow.core.crypto.aes.AESKeyService;
import com.vastmoon.sparrow.core.crypto.aes.AESManager;
import com.vastmoon.sparrow.core.crypto.rsa.RSAManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

/**
 * 加密算法自动配置配置类
 *
 * @author yousuf 2020/12/31 11:02
 **/
@Slf4j
@Configuration(proxyBeanMethods = false)
@Import(JwtAutoConfiguration.class)
@EnableConfigurationProperties(CryptoProperties.class)
public class CryptoAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public RSAManager rsaManager(CryptoProperties properties) {
        log.info("init default RSAManager");
        return new RSAManager(properties.getRsa());
    }

    @Bean
    @ConditionalOnMissingBean
    @DependsOn(AESKeyService.AES_KEY_SERVICE_NAME)
    public AESManager aesManager(CryptoProperties properties, AESKeyService aesKeyService) {
        log.info("init default AesHelper");
        return new AESManager(properties.getAes(), aesKeyService);
    }

    @Bean(name = AESKeyService.AES_KEY_SERVICE_NAME)
    @ConditionalOnMissingBean
    public AESKeyService aesKeyService() {
        log.info("init default AesKeyService");
        return name -> {
            log.info("默认实现" + name );
            return null;
        };
    }

}
