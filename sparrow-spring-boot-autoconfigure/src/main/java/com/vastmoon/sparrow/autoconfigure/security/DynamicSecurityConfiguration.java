package com.vastmoon.sparrow.autoconfigure.security;

import com.vastmoon.sparrow.client.dto.security.MetaResource;
import com.vastmoon.sparrow.client.dto.security.RoleResource;
import com.vastmoon.sparrow.core.security.dynamic.DynamicFilterInvocationSecurityMetadataSource;
import com.vastmoon.sparrow.core.security.dynamic.MetaResourceProcessor;
import com.vastmoon.sparrow.core.security.dynamic.RequestMatcherConverter;
import com.vastmoon.sparrow.core.security.dynamic.RoleProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 动态权限自动配置类
 *
 * @author yousuf 2021/1/7 16:42
 **/
@Slf4j

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(DynamicSecurityProperties.class)
@ConditionalOnProperty(prefix = "vastmoon.security.dynamic", name = "enabled", havingValue = "true")
public class DynamicSecurityConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public RequestMatcherConverter requestMatcherConverter() {
        return metaResources -> metaResources.stream()
                .map(metaResource -> new AntPathRequestMatcher(metaResource.getPattern(), metaResource.getMethod()))
                .collect(Collectors.toSet());
    }
    @Bean
    @ConditionalOnMissingBean
    public MetaResourceProcessor metaResourceProcessor(DynamicSecurityProperties properties) {
        return () -> properties.getRoles().stream()
                .map(RoleResource::getResource)
                .collect(Collectors.toSet());
    }

    @Bean
    @ConditionalOnMissingBean
    public RoleProcessor roleProcessor(DynamicSecurityProperties properties) {
        return new RoleProcessor() {
            @Override
            public Set<String> queryRoleByPattern(MetaResource resource) {
                return properties.getRoles().stream().filter(role -> Objects.equals(role.getResource(), resource))
                        .map(RoleResource::getRole)
                        .collect(Collectors.toSet());
            }

            @Override
            public Set<String> queryAllAvailable() {
                return properties.getRoles().stream().map(RoleResource::getRole)
                        .collect(Collectors.toSet());
            }
        };
    }

    @Bean
    @ConditionalOnMissingBean
    public FilterInvocationSecurityMetadataSource dynamicFilterInvocationSecurityMetadataSource(RequestMatcherConverter requestMatcherConverter,
                                                                                                MetaResourceProcessor metaResourceProcessor,
                                                                                                RoleProcessor roleProcessor) {
        return new DynamicFilterInvocationSecurityMetadataSource(requestMatcherConverter, metaResourceProcessor, roleProcessor);
    }

    @Bean
    @ConditionalOnMissingBean
    public RoleVoter roleVoter(DynamicSecurityProperties properties) {
        RoleVoter roleVoter = new RoleVoter();
        roleVoter.setRolePrefix(properties.getRolePrefix());
        return roleVoter;
    }

    @Bean
    @ConditionalOnMissingBean
    public AccessDecisionManager affirmativeBased(List<AccessDecisionVoter<?>> decisionVoters) {
        return new AffirmativeBased(decisionVoters);
    }

}
