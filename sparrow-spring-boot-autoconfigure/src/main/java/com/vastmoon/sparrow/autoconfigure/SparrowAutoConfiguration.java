package com.vastmoon.sparrow.autoconfigure;

import com.vastmoon.sparrow.core.common.ApplicationContextHolder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * core初始化配置
 *
 * @author yousuf 2020/12/30 17:09
 **/
@Configuration(proxyBeanMethods = false)
public class SparrowAutoConfiguration {
    @Bean
    public ApplicationContextHolder applicationContextHolder() {
        return new ApplicationContextHolder();
    }
}
