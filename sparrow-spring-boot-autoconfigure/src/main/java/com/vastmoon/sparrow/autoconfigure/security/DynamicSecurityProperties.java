package com.vastmoon.sparrow.autoconfigure.security;

import com.google.common.collect.Sets;
import com.vastmoon.sparrow.client.dto.security.RoleResource;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Set;

/**
 * security配置项
 *
 * @author yousuf 2021/1/7 16:37
 **/
@Data
@ConfigurationProperties(prefix = "vastmoon.security.dynamic")
public class DynamicSecurityProperties {
    private boolean enabled;
    private Set<RoleResource> roles = Sets.newHashSet();
    private String rolePrefix = "";
}
