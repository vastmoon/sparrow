package com.vastmoon.sparrow.gradle;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.springframework.boot.gradle.plugin.SpringBootPlugin;

/**
 * 重写springbootPlugin 方便统一版本控制
 * @author yousuf 2020/12/17 15:44
 **/
public class SparrowGradlePlugin implements Plugin<Project> {
    public static final String BOM_COORDINATES = "com.vastmoon.sparrow:sparrow-dependencies:";
    @Override
    public void apply(Project project) {
        project.getPlugins().apply(SpringBootPlugin.class);
    }
}
