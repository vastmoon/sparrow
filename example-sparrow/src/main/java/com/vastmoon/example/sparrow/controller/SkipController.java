package com.vastmoon.example.sparrow.controller;

import com.vastmoon.sparrow.core.security.annotation.Skip;
import org.springframework.web.bind.annotation.*;

/**
 * 测试跳过登录或者匿名访问链接
 *
 * @author yousuf 2021/1/19 13:32
 **/
@RestController
@RequestMapping("/skip")
public class SkipController {

    @Skip(logged = false)
    @GetMapping("/test/{id}")
    public String testById(@PathVariable("id") Long id) {
        return "skip test id: " + id;
    }

    @Skip
    @PostMapping("/test")
    public String testAll() {
        return "test All";
    }

}
