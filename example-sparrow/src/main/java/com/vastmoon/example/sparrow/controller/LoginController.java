package com.vastmoon.example.sparrow.controller;

import com.vastmoon.sparrow.client.dto.rest.Rest;
import com.vastmoon.sparrow.client.dto.rest.RestBody;
import com.vastmoon.sparrow.core.security.CustomizeSecurityCode;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录
 *
 * @author yousuf 2021/1/5 09:47
 **/
@RestController
@RequestMapping("/login")
public class LoginController {
    @PostMapping("/failure")
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Rest<Void> loginFailure() {
        return RestBody.failure(CustomizeSecurityCode.UNAUTHORIZED);
    }

    @PostMapping("/success")
    public Rest<User> loginSuccess() {
        // 登录成功后用户的认证信息 UserDetails会存在 安全上下文寄存器 SecurityContextHolder 中
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.getUsername();
        return RestBody.ok(principal);
    }
}
