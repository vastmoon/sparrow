package com.vastmoon.example.sparrow.config;

import com.vastmoon.sparrow.core.security.SecurityConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

/**
 * 自定义用户管理类
 *
 * @author yousuf 2021/1/4 15:12
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class CustomizeUserDetailsManager implements UserDetailsManager {
    private final PasswordEncoder passwordEncoder;
    @Override
    public void createUser(UserDetails user) {

    }

    @Override
    public void updateUser(UserDetails user) {
    }

    @Override
    public void deleteUser(String username) {

    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {

    }

    @Override
    public boolean userExists(String username) {
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return User.builder()
                .username(username)
                .password("{noop}123456")
                .authorities(AuthorityUtils.createAuthorityList("user",
                        SecurityConstants.ROLE_ANONYMOUS,
                        SecurityConstants.ROLE_SKIP))
                .build();
    }
}
