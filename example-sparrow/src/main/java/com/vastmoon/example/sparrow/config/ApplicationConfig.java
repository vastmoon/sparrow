package com.vastmoon.example.sparrow.config;

import org.springframework.context.annotation.Configuration;

/**
 * 系统配置
 *
 * @author yousuf 2021/1/5 09:32
 **/
@Configuration(proxyBeanMethods = false)
public class ApplicationConfig {

}
