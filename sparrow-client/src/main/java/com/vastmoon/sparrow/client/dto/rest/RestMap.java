package com.vastmoon.sparrow.client.dto.rest;

import com.vastmoon.sparrow.client.dto.BaseDTO;

import java.util.Map;

/**
 * restMap接口
 *
 * @author yousuf 2020/12/18 17:25
 **/
public interface RestMap extends BaseDTO {
    /**
     * 用于{@link RestBody} 转换为map用的接口
     * @return RestMap 转换为map
     */
    Map<String, Object> toMap();

    /**
     * 获取值
     * @param key key
     * @return rest返回的值
     */
    public Object get(String key);
}
