package com.vastmoon.sparrow.client.dto.rest;

import com.vastmoon.sparrow.client.dto.SuccessCode;
import com.vastmoon.sparrow.client.util.StringUtils;
import lombok.Getter;
import lombok.ToString;

/**
 * rest 风格返回信息统一封装
 * @author yousuf 2020/12/18 09:18
 **/
@Getter
@ToString
public final class RestBody<T> implements Rest<T> {
    private static final long serialVersionUID = 838412466822978449L;
    private final String code;
    private final String message;
    private final T data;

    public RestBody(String code, String message, T data) {
        if (StringUtils.isEmpty(code)) {
            throw new NullPointerException("code must not be null");
        }
        if (StringUtils.isEmpty(message)) {
            throw new NullPointerException("message must not bu null");
        }
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public RestBody(RestCode code, T data) {
        this(code.getCode(), code.getMessage(), data);
    }

    public RestBody(RestCode code) {
        this(code, null);
    }

    public RestBody() {
        this(SuccessCode.SUCCESS);
    }

    public RestBody(T data) {
        this(SuccessCode.SUCCESS, data);
    }

    /**
     * 返回成功信息
     * @param <T> 任意类型数据
     * @return 成功信息
     */
    public static <T> Rest<T> ok() {
        return new RestBody<>();
    }

    /**
     * 返回带数据的成功信息
     * @param data 返回数据
     * @param <T> 任意类型数据
     * @return rest 成功信息
     */
    public static <T> Rest<T> ok(T data) {
        return new RestBody<>(data);
    }

    /**
     * 业务码错误信息
     * @param code 业务码
     * @param <T> 任意类型数据
     * @return rest 异常信息
     */
    public static <T> Rest<T> failure(RestCode code) {
        return new RestBody<>(code);
    }
}
