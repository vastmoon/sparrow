package com.vastmoon.sparrow.client.dto.rest;

import lombok.ToString;

import java.util.Collections;
import java.util.Map;

/**
 * 用于处理自定义返回信息
 *
 * @author yousuf 2020/12/18 12:52
 **/
@ToString
public final class DefaultRestMap implements RestMap {
    private static final long serialVersionUID = -62858114542550649L;
    private final Map<String, Object> rest;

    protected DefaultRestMap(Map<String, Object> rest) {
        this.rest = Collections.unmodifiableMap(rest);
    }

    @Override
    public Map<String, Object> toMap() {
        return this.rest;
    }

    @Override
    public Object get(String key) {
        return this.rest.get(key);
    }
}
