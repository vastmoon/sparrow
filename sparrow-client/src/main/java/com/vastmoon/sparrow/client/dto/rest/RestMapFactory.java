package com.vastmoon.sparrow.client.dto.rest;

import lombok.experimental.UtilityClass;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * RestMapFactory 工厂类
 *
 * @author yousuf 2020/12/21 18:14
 **/
@UtilityClass
public class RestMapFactory {

    /**
     * rest 转换为 restMap
     * @param rest rest
     * @return restMap
     */
    public RestMap of(Rest<?> rest) {
        return of(rest, Collections.emptyMap());
    }

    /**
     * rest 转换为 restMap
     * @param rest 返回信息 {@link Rest}
     * @param additionalInfo 附加信息
     * @return 转换为 {@link RestMap}
     */
    public RestMap of(Rest<?> rest, Map<String, Object> additionalInfo) {
        Map<String, Object> dataMap = new HashMap<>(16);
        dataMap.put("code", rest.getCode());
        dataMap.put("message", rest.getMessage());
        if (Objects.nonNull(rest.getData())) {
            dataMap.put("data", rest.getData());
        }
        if (Objects.nonNull(additionalInfo)) {
            dataMap.putAll(additionalInfo);
        }
        return new DefaultRestMap(dataMap);
    }

    /**
     * 返回错误信息
     * @param rest rest
     * @param errorCode 错误代码
     * @param errorMessage 错误原因
     * @return 转换后的 RestMap
     */
    public RestMap of(Rest<?> rest, String errorCode, Object errorMessage) {
        Map<String, Object> additionalInfo = new HashMap<>(1);
        Map<String, Object> error = new HashMap<>(1);
        error.put(errorCode, errorMessage);
        additionalInfo.put("error", error);
        return of(rest, additionalInfo);
    }

    /**
     * 返回错误信息
     * @param rest rest
     * @param errors 错误信息
     * @return 错误信息
     */
    public RestMap ofErrors(Rest<?> rest, Map<String, Object> errors) {
        Map<String, Object> additionalInfo = new HashMap<>(1);
        additionalInfo.put("error", errors);
        return of(rest, additionalInfo);
    }

    public RestMap ofData(Rest<?> rest, Map<String, Object> data) {
        Map<String, Object> dataMap = new HashMap<>(3);
        dataMap.put("code", rest.getCode());
        dataMap.put("message", rest.getMessage());
        dataMap.put("data", data);
        return new DefaultRestMap(dataMap);
    }


}
