package com.vastmoon.sparrow.client.dto.page;

import com.vastmoon.sparrow.client.util.StringUtils;
import lombok.experimental.UtilityClass;

import java.util.*;

/**
 * OrderDesc静态工厂类
 *
 * @author yousuf 2020/12/25 16:24
 **/
@UtilityClass
public class OrderDescFactory {
    private final String NAME_SEPARATOR = "\\.";
    private final String SEPARATOR = "-";
    private final int SEPARATOR_LENGTH = 2;
    private final String DESC = "descend";
    private final String ASC = "ascend";

    /**
     * 转换排序字段 支持多字段排序.
     * <p>字段.descend/ascend 如 username.descend</p>
     * <p>多字段排序用"-"隔开 如 username.descend-age.ascend</p>
     *
     * @param sort 排序字符串
     * @return 排序 list
     */
    public List<OrderDesc> toMultiSort(String sort) {
        if(StringUtils.isNotEmpty(sort)) {
            List<String> multiSort = Arrays.asList(sort.split(SEPARATOR).clone());
            return multiSort(multiSort);
        }
        return Collections.emptyList();
    }

    /**
     * 多字段趴下
     * @param multiSort 排序list
     * @return 排序
     */
    public List<OrderDesc> multiSort(List<String> multiSort) {
        List<OrderDesc> orders = new LinkedList<>();
        if (Objects.nonNull(multiSort) && !multiSort.isEmpty()) {
            for (String multi: multiSort) {
                String[] order = multi.split(NAME_SEPARATOR);
                addOrderDesc(orders, order);
            }
        }
        return orders;
    }

    private void addOrderDesc(List<OrderDesc> orders, String[] order) {
        OrderDesc orderDesc = null;
        if (order.length == SEPARATOR_LENGTH) {
            if (Objects.equals(ASC, order[1])) {
                orderDesc = OrderDesc.asc(order[0]);
            } else if (Objects.equals(DESC, order[1])) {
                orderDesc = OrderDesc.desc(order[0]);
            }
        }
        if (Objects.nonNull(orderDesc) && !orders.contains(orderDesc)) {
            orders.add(orderDesc);
        }
    }
}

