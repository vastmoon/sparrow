package com.vastmoon.sparrow.client.dto.rest;

/**
 * <p> ClassName: RestCode
 * <p> Description: 返回码接口
 *
 * @author yousuf 2020/11/5
 */
public interface RestCode {
    /**
     * 返回业务码
     *
     * @return 状态码
     */
    String getCode();

    /**
     * 返回业务处理描述信息
     * @return 信息
     */
    String getMessage();
}
