package com.vastmoon.sparrow.client.dto.security;

import com.vastmoon.sparrow.client.dto.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 权限资源
 *
 * @author yousuf 2021/1/7 15:59
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetaResource implements BaseDTO {
    private static final long serialVersionUID = 5954289354744125746L;
    private String pattern;
    private String method;
}
