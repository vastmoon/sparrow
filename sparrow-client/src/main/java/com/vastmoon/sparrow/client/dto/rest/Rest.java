package com.vastmoon.sparrow.client.dto.rest;

import com.vastmoon.sparrow.client.dto.BaseDTO;

/**
 * <p> ClassName: Rest
 * <p> Description: rest接口
 *
 * @author yousuf 2020/11/6
 */
public interface Rest<T> extends BaseDTO {
    /**
     * 得到返回的业务码
     * @return 业务码
     */
    String getCode();

    /**
     * 得到业务信息描述
     * @return 业务信息
     */
    String getMessage();

    /**
     * 得到数据信息
     * @return 数据
     */
    T getData();
}
