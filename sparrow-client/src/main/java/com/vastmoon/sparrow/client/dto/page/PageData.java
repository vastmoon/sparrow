package com.vastmoon.sparrow.client.dto.page;

import com.vastmoon.sparrow.client.dto.BaseDTO;
import lombok.Data;

import java.util.Collection;
import java.util.List;

/**
 * 分页数据
 *
 * @author yousuf 2020/12/25 15:31
 **/
@Data
public class PageData<T> implements BaseDTO {
    private static final long serialVersionUID = 6814671161313483451L;
    private Long total;
    private Integer pageNum;
    private Integer pageSize;
    private Integer offset;
    private Collection<T> data;

    public static <T> PageData<T> of(List<T> data, Long total) {
        PageData<T> pageData = new PageData<>();
        pageData.setData(data);
        pageData.setTotal(total);
        return pageData;
    }
}
