package com.vastmoon.sparrow.client.dto.security;

import com.vastmoon.sparrow.client.dto.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色资源
 *
 * @author yousuf 2021/1/7 17:37
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleResource implements BaseDTO {
    private static final long serialVersionUID = -1569604144231398527L;
    private String role;
    private MetaResource resource;
}
