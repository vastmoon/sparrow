package com.vastmoon.sparrow.client.dto;

import com.vastmoon.sparrow.client.dto.rest.RestCode;
import lombok.Getter;

/**
 * <p> ClassName: SuccessCode
 * <p> Description: 成功信息返回
 *
 * @author yousuf 2020/11/5
 */
public enum SuccessCode implements RestCode {
    /**成功*/
    SUCCESS("SUCCESS", "Success"),
    ;
    @Getter
    private final String code;
    @Getter
    private final String message;

    SuccessCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
