package com.vastmoon.sparrow.client.dto;

import java.io.Serializable;

/**
 * dto接口
 *
 * @author yousuf 2020/12/18 09:04
 **/
public interface BaseDTO extends Serializable {
}
