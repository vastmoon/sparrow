package com.vastmoon.sparrow.client.dto.page;

import com.vastmoon.sparrow.client.dto.BaseDTO;
import com.vastmoon.sparrow.client.util.StringUtils;
import lombok.Data;

import java.util.List;

/**
 * 分页查询条件
 *
 * @author yousuf 2020/12/25 14:38
 **/
@Data
public class PageQuery implements BaseDTO {
    private static final long serialVersionUID = -6849117702649777604L;
    private int pageNum = 1;
    private int pageSize = 10;
    private boolean needTotalCount = true;
    private String sort;
    private List<String> multiSort;

    /**
     * 获取当前页数 首页从0开始.
     * 无效输入视为第一页
     * @return 当前页数
     */
    public int getCurrentPage() {
        return this.pageNum > 0 ? (this.pageNum -1) : 0;
    }

    /**
     * 获取分页偏移量
     * @return 偏移量
     */
    public int getOffset() {
        return getCurrentPage() * pageSize;
    }

    /**
     * 转换排序为 {@link OrderDesc} 类型
     * 排序字段格式参照 {@link OrderDescFactory#toMultiSort(String)}
     * @return 排序字段集合
     */
    public List<OrderDesc> orderDesc() {
        return StringUtils.isNotEmpty(sort) ? OrderDescFactory.toMultiSort(sort) : OrderDescFactory.multiSort(this.multiSort);
    }




}
