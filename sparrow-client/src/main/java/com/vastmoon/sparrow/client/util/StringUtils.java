package com.vastmoon.sparrow.client.util;

import lombok.experimental.UtilityClass;

/**
 * string 辅助类
 *
 * @author yousuf 2020/12/18 14:06
 **/
@UtilityClass
public class StringUtils {
    /**
     * 判断字符串是否为空
     * @param cs 值
     * @return 是否为空判断
     */
    public boolean isEmpty(final CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    /**
     * 判断字符串是否非空
     * @param cs 值
     * @return 是否非空
     */
    public boolean isNotEmpty(final CharSequence cs) {
        return !isEmpty(cs);
    }
}
