package com.vastmoon.sparrow.client.dto.page;

import com.vastmoon.sparrow.client.dto.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 排序字段
 *
 * @author yousuf 2020/12/25 14:43
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDesc implements BaseDTO {
    private static final long serialVersionUID = -1003917440196809674L;
    private String col;
    private boolean desc = true;
    /**
     * 获取倒序排序
     * @param col 列
     * @return 排序
     */
    public static OrderDesc desc(String col) {
        return new OrderDesc(col, true);
    }

    /**
     * 获取正序排序
     * @param col 列
     * @return 正序排序
     */
    public static OrderDesc asc(String col) {
        return new OrderDesc(col, false);
    }

    @Override
    public String toString() {
        return this.desc ? col +" desc" : col + " asc";
    }

}
