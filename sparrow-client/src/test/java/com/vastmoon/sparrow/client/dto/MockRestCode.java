package com.vastmoon.sparrow.client.dto;

import com.vastmoon.sparrow.client.dto.rest.RestCode;

/**
 * 用于测试Rest返回错误信息
 * @author yousuf 2020/12/18 10:00
 **/
public enum MockRestCode implements RestCode {
    MOCK_ERROR("mock_error", "Mock 错误信息");
    private final String code;
    private final String message;

    MockRestCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
