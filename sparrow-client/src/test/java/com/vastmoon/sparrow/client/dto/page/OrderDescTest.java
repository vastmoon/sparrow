package com.vastmoon.sparrow.client.dto.page;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 测试类
 *
 * @author yousuf 2020/12/25 14:45
 **/
class OrderDescTest {

    @Test
    void when_new_desc_return_desc_sort() {
        OrderDesc desc = OrderDesc.desc("user_name");
        assertTrue(desc.isDesc());
        assertEquals(desc.getCol(), "user_name");
        assertEquals(desc.toString(), "user_name desc");
        OrderDesc asc = OrderDesc.asc("name");
        assertFalse(asc.isDesc());

    }

}