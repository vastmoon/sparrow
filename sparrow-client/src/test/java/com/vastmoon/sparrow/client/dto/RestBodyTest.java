package com.vastmoon.sparrow.client.dto;

import com.vastmoon.sparrow.client.dto.rest.*;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <p>
 * Rest 接口测试类
 * </p>
 *
 * @author yousuf 2020/12/18 09:20
 **/
class RestBodyTest {

    @Test
    void when_200_then_return_success() {
        Rest<Void> rest = RestBody.ok();
        assertEquals(rest.getCode(), SuccessCode.SUCCESS.getCode());
        assertEquals(rest.getMessage(), SuccessCode.SUCCESS.getMessage());
    }

    @Test
    void when_error_rest_code_then_return_error() {
        RestCode restCode = MockRestCode.MOCK_ERROR;
        Rest<Void> rest = RestBody.failure(restCode);
        assertEquals(rest.getCode(), MockRestCode.MOCK_ERROR.getCode());
        assertEquals(rest.getMessage(), MockRestCode.MOCK_ERROR.getMessage());
    }

    @Test
    void when_data_then_return_data_message() {
        String data = "test return data";
        Rest<String> rest = RestBody.ok(data);
        assertEquals(rest.getData(), data);
        assertEquals(rest.getCode(), SuccessCode.SUCCESS.getCode());
        assertEquals(rest.getMessage(), SuccessCode.SUCCESS.getMessage());
    }

    @Test
    void when_error_data_then_return_error_map() {
        Rest<Void> rest = RestBody.failure(MockRestCode.MOCK_ERROR);
        Map<String, Object> additionalInfo = new HashMap<>(1);
        additionalInfo.put("error", "test additional information");
        RestMap map = RestMapFactory.of(rest, additionalInfo);
        assertEquals(map.get("code"), MockRestCode.MOCK_ERROR.getCode());
        assertEquals(map.get("message"), MockRestCode.MOCK_ERROR.getMessage());
        assertEquals(map.get("error"), "test additional information");

        map = RestMapFactory.of(rest, "test", "test");
        System.out.println(map);
    }
}