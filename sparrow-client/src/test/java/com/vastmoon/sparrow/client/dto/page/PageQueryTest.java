package com.vastmoon.sparrow.client.dto.page;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * 分页查询测试
 *
 * @author yousuf 2020/12/25 14:53
 **/
class PageQueryTest {

    @Test
    void when_new_page_query_then_return_page_query() {
        PageQuery query = new PageQuery();
        assertEquals(query.getCurrentPage(), 0);
        assertEquals(query.getPageSize(), 10);
        assertEquals(query.getOffset(), 0);
        query.setPageNum(2);
        assertEquals(query.getCurrentPage(), 1);
        query.setPageSize(20);
        assertEquals(query.getOffset(), 20);
    }

    @Test
    void when_string_sort_then_return_list_sort() {
        PageQuery query = new PageQuery();
        query.setSort("user_name.descend-age.ascend-user_name.descend");
        List<OrderDesc> desc = query.orderDesc();
        assertEquals(desc.size(), 2);
        assertTrue(desc.contains(OrderDesc.desc("user_name")));
        assertTrue(desc.contains(OrderDesc.asc("age")));
    }

    @Test
    void when_list_sort_then_return_list_sort() {
        List<String> sorts = new ArrayList<>();
        sorts.add("user_name.descend");
        sorts.add("age.ascend");
        PageQuery query = new PageQuery();
        query.setMultiSort(sorts);
        List<OrderDesc> sorted = query.orderDesc();
        assertEquals(sorted.size(), 2);
        assertTrue(sorted.contains(OrderDesc.desc("user_name")));
        assertTrue(sorted.contains(OrderDesc.asc("age")));
    }

}